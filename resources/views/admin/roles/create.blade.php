@extends('admin.layout')
@section('content')
    <section class="panel">
        <header class="panel-heading">
            Create Role
        </header>
        <div class="panel-body">
            @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            <!-- Add error message -->
            @if(session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="form">
                <form class="cmxform form-horizontal" id="commentForm" enctype="multipart/form-data"
                      method="post" action="{{ route('roles.store') }}" novalidate="novalidate">
                    @csrf
                    <div class="form-group">
                        <label for="cname" class="control-label col-lg-3">Name </label>
                        <div class="col-lg-6">
                            <input class="form-control" id="cname" name="name" minlength="2" type="text"
                                   value=" {{ old('name') }}" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="control-label col-lg-3">Description </label>
                        <div class="col-lg-6">
                            <input class="form-control" id="cemail" type="email" value=" {{ old('description') }}"
                                   name="description" required>
                        </div>
                    </div>
                    <div class="border-dark p-3">
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="row">
                                    @foreach($permissions as $permissionsParentItem)
                                        <div class="card border-primary mb-3 col-md-12">
                                            <div class="card-header">
                                                <label>
                                                    <input type="checkbox" value="" class="checkbox_wrapper">
                                                </label> Module {{$permissionsParentItem->name}}
                                            </div>
                                            <div class="row">
                                                @foreach($permissionsParentItem->permissionsChildrent as $permissionChildrenItem )
                                                    <div class="card-body text-primary col-md-3">
                                                        <h5 class="card-title">
                                                            <label>
                                                                <input type="checkbox" class="checkbox_children"
                                                                       name="permission_id[]"
                                                                       value="{{$permissionChildrenItem->id}}"
                                                                       @if(is_array(old('permission_id')) && in_array($permissionChildrenItem->id, old('permission_id')))
                                                                           checked
                                                                    @endif
                                                                >
                                                            </label>
                                                            {{$permissionChildrenItem->name}}
                                                        </h5>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-3 col-lg-6">
                            <button class="btn btn-primary" type="submit">Save</button>
                            <button class="btn btn-default" type="button">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
