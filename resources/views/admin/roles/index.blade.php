@extends('admin.layout')
@section('content')

    <section class="wrapper">
        <div class="table-agile-info">
            <div class="panel panel-default">
                <div class="panel-heading">
                    List Roles
                </div>
                <div class="row w3-res-tb">
                    <div class="col-sm-4">
                    </div>
                    <div class="col-sm-3 search-key">
                        <div class="input-group">
                            <form action="{{ route('roles.index') }}" method="GET">
                                <input type="text" class="input-sm form-control" placeholder="Search" name="keyword"
                                       value="{{ $keyword }}">
                                <div class="input-group-btn">
                                    <button class="btn btn-sm btn-default" type="submit">Search</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                @if ($roles->isEmpty())
                    <div class="alert alert-info">
                        Not have data for keyword: {{ $keyword }}
                    </div>
                @else
                    <div class="table-responsive">
                        @if(session()->has('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif

                        @if(session()->has('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                        @endif

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <table class="table table-striped b-t b-light">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th style="width: 30px;"></th>
                                <th style="width: 30px;"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($roles as $role)
                                <tr id="tr_{{$role->id}}">
                                    <td>{{ $role->id }}</td>
                                    <td><span class="text-ellipsis">{{ $role->name }}</span></td>
                                    <td><span class="text-ellipsis">{{ $role->description }}</span></td>
                                    <td>
                                        <a href="{{ route('roles.edit', ['role_id' => $role->id]) }}"
                                           class="btn btn-primary">Edit</a>
                                    </td>
                                    <td>
                                    <td>
                                        <a href="javascript:void(0)" onclick="deleteItem({{$role->id}}, 'roles')"
                                           class="btn btn-danger delete-role">Delete</a>
                                    </td>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="5">No roles available.</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                @endif

                <footer class="panel-footer">
                    <div class="row">
                        <div class="col-sm-7 text-right text-center-xs">
                            <ul class="pagination pagination-sm m-t-none m-b-none">
                                @if ($roles->onFirstPage())
                                    <li class="disabled"><span>&laquo;</span></li>
                                @else
                                    <li><a href="{{ $roles->previousPageUrl() }}" rel="prev">&laquo;</a></li>
                                @endif
                                @for ($i = 1; $i <= $roles->lastPage(); $i++)
                                    <li class="{{ ($roles->currentPage() == $i) ? 'active' : '' }}">
                                        <a href="{{ $roles->url($i) }}">{{ $i }}</a>
                                    </li>
                                @endfor
                                @if ($roles->hasMorePages())
                                    <li><a href="{{ $roles->nextPageUrl() }}" rel="next">&raquo;</a></li>
                                @else
                                    <li class="disabled"><span>&raquo;</span></li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </footer>

            </div>
        </div>
@endsection
