<!-- resources/views/admin/products/edit.blade.php -->

@extends('admin.layout')
@section('content')
    <section class="panel">
        <header class="panel-heading">
            Edit Category
        </header>
        <div class="panel-body">
            @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            @if(session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="form">
                <form class="cmxform form-horizontal" id="commentForm" method="post" enctype="multipart/form-data"
                      action="{{ route('categories.update', $category->id) }}" novalidate="novalidate">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="cname" class="control-label col-lg-3">Name </label>
                        <div class="col-lg-6">
                            <input class="form-control" id="cname" name="name" minlength="2" type="text"
                                   value="{{ old('name',$category->name) }}" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="curl" class="control-label col-lg-3">Product</label>
                        <div class="col-lg-6">
                            <select name="products[]" class="form-control select2_init" multiple>
                                @foreach($products as $product)
                                    <option
                                            {{ (collect(old('products', $productOfCategory->pluck('id')))->contains($product->id)) ? 'selected' : '' }}
                                            value="{{ $product->id }}">{{ $product->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-3 col-lg-6">
                            <button class="btn btn-primary" type="submit">Update</button>
                            <a href="{{ route('categories.index') }}" class="btn btn-default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
