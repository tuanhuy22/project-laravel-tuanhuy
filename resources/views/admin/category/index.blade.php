@extends('admin.layout')
@section('content')

    <section class="wrapper">
        <div class="table-agile-info">
            <div class="panel panel-default">
                <div class="panel-heading">
                    List Category
                </div>
                <div class="table-responsive">
                    @if(session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                    @if(session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <table class="table table-striped b-t b-light">
                        <thead>
                        <tr>

                            <th> ID</th>
                            <th> Name</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($categories as $category)
                            <tr id="tr_{{$category->id}}">
                                <td>{{$category->id}}</td>
                                <td><span class="text-ellipsis">{{ $category->name }}</span></td>
                                @can('update-category')
                                    <td>
                                        <a href="{{ route('categories.edit', ['category_id' => $category->id]) }}"
                                           class="btn btn-primary">Edit</a>
                                    </td>
                                @endcan
                                @can('delete-category')
                                    <td>
                                        <a href="javascript:void(0)" onclick="deleteItem({{$category->id}}, 'categories')"
                                           class="btn btn-danger delete-category">Delete</a>
                                    </td>
                                @endcan
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <footer class="panel-footer">
                    <div class="row">
                        <div class="col-sm-7 text-right text-center-xs">
                            <ul class="pagination pagination-sm m-t-none m-b-none">
                                @if ($categories->onFirstPage())
                                    <li class="disabled"><span>&laquo;</span></li>
                                @else
                                    <li><a href="{{ $categories->previousPageUrl() }}" rel="prev">&laquo;</a></li>
                                @endif
                                @for ($i = 1; $i <= $categories->lastPage(); $i++)
                                    <li class="{{ ($categories->currentPage() == $i) ? 'active' : '' }}">
                                        <a href="{{ $categories->url($i) }}">{{ $i }}</a>
                                    </li>
                                @endfor
                                @if ($categories->hasMorePages())
                                    <li><a href="{{ $categories->nextPageUrl() }}" rel="next">&raquo;</a></li>
                                @else
                                    <li class="disabled"><span>&raquo;</span></li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </footer>

            </div>
        </div>

@endsection
