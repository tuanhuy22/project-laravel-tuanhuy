@extends('admin.layout')
@section('content')
    <section class="panel">
        <header class="panel-heading">
            Create Category
        </header>
        <div class="panel-body">
            @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            @if(session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="form">
                <form class="cmxform form-horizontal" id="commentForm" method="post" enctype="multipart/form-data"
                      action="{{ route('categories.store') }}" novalidate="novalidate">
                    @csrf
                    <div class="form-group">
                        <label for="cname" class="control-label col-lg-3">Name </label>
                        <div class="col-lg-6">
                            <input class="form-control" id="cname" name="name" minlength="2" value="{{ old('name') }}"
                                   type="text" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="curl" class="control-label col-lg-3">Product</label>
                        <div class="col-lg-6">
                            <select name="products[]" class="form-control select2_init" multiple>
                                @foreach($products as $product)
                                    <option
                                        value="{{ $product->id }}" {{ in_array($product->id, old('products', [])) ? 'selected' : '' }}>
                                        {{ $product->name }}
                                    </option>
                                @endforeach
                            </select>

                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-3 col-lg-6">
                            <button class="btn btn-primary" type="submit">Save</button>
                            <button class="btn btn-default" type="button">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
