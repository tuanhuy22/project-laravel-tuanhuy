@extends('admin.layout')
@section('content')
    <section class="wrapper">
        <div class="table-agile-info">
            <div class="panel panel-default">
                <div class="panel-heading">
                    List Product
                </div>
                <div class="row w3-res-tb form-search">
                    <div class="col-sm-6">
                        <div class="input-group">
                            <form action="{{ route('products.index') }}" method="GET">
                                <div class="form-group">
                                    <label for="min_price">Min Price:</label>
                                    <input type="number" class="input-sm form-control" name="min_price" id="min_price"
                                           placeholder="Min Price" value="{{ $minPrice }}">
                                </div>
                                <div class="form-group">
                                    <label for="max_price">Max Price:</label>
                                    <input type="number" class="input-sm form-control" name="max_price" id="max_price"
                                           placeholder="Max Price" value="{{ $maxPrice }}">
                                </div>
                                <div class="form-group">
                                    <label for="keyword">Keyword:</label>
                                    <input type="text" class="input-sm form-control" id="keyword" placeholder="Search"
                                           name="keyword" value="{{ $keyword }}">
                                </div>
                                <div class="form-group">
                                    <label for="curl" class="control-label ">Category :</label>
                                    <select name="categories[]" class="form-control select2_init" multiple>
                                        @foreach($categoriesList as $category)
                                            <option
                                                value="{{ $category->id }}" {{ in_array($category->id, (array)request('categories', [])) ? 'selected' : '' }}>
                                                {{ $category->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-success" type="submit">Search</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            @if ($products->isEmpty())
                <div class="alert alert-info">
                    No results found.
                </div>
            @else
                <div class="table-responsive">
                    @if(session()->has('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif

                    @if(session()->has('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <table class="table table-striped b-t b-light">
                        <thead>
                        <tr>
                            <th> ID</th>
                            <th> Name</th>
                            <th> Image</th>
                            <th> Description</th>
                            <th> Price</th>
                            @can('update-product')
                                <th> Edit</th>
                            @endcan
                            @can('delete-product')
                                <th> Delete</th>
                            @endcan
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($products as $product)
                            <tr id="tr_{{$product->id}}">
                                <td>{{ $product->id }}</td>
                                <td><span class="text-ellipsis">{{ $product->name }}</span></td>
                                <td>
                                    @if($product->image_path)
                                        <img src="{{ asset($product->image_path) }}" alt="{{ $product->name }}"
                                             class="image">
                                    @else
                                        No Image
                                    @endif
                                </td>
                                <td><span class="text-ellipsis">{{ $product->description }}</span></td>
                                <td><span class="text-ellipsis">{{ $product->formatted_price }}</span></td>
                                @can('update-product')
                                    <td>
                                        <a href="{{ route('products.edit', ['product_id' => $product->id]) }}"
                                           class="btn btn-primary">Edit</a>
                                    </td>
                                @endcan
                                @can('delete-product')
                                    <td>
                                        <a href="javascript:void(0)" onclick="deleteItem({{$product->id}}, 'products')"
                                           class="btn btn-danger delete-product">Delete</a>
                                    </td>
                                @endcan
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6">No products available.</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            @endif
            <footer class="panel-footer">
                <div class="row">
                    <div class="col-sm-7 text-right text-center-xs">
                        <ul class="pagination pagination-sm m-t-none m-b-none">
                            @if ($products->onFirstPage())
                                <li class="disabled"><span>&laquo;</span></li>
                            @else
                                <li><a href="{{ $products->previousPageUrl() }}" rel="prev">&laquo;</a></li>
                            @endif
                            @for ($i = 1; $i <= $products->lastPage(); $i++)
                                <li class="{{ ($products->currentPage() == $i) ? 'active' : '' }}">
                                    <a href="{{ $products->url($i) }}">{{ $i }}</a>
                                </li>
                            @endfor
                            @if ($products->hasMorePages())
                                <li><a href="{{ $products->nextPageUrl() }}" rel="next">&raquo;</a></li>
                            @else
                                <li class="disabled"><span>&raquo;</span></li>
                            @endif
                        </ul>
                    </div>
                </div>
            </footer>

        </div>
        </div>
    </section>
@endsection
