@extends('admin.layout')
@section('content')
    <section class="panel">
        <header class="panel-heading">
            Create Product
        </header>
        <div class="panel-body">
            @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            @if(session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="form">
                <form class="cmxform form-horizontal" id="commentForm" method="POST" enctype="multipart/form-data"
                      action="{{ route('products.store') }}" novalidate="novalidate">
                    @csrf
                    <div class="form-group">
                        <label for="cname" class="control-label col-lg-3">Name </label>
                        <div class="col-lg-6">
                            <input class="form-control" id="cname" name="name" minlength="2" type="text"
                                   value="{{ old('name') }}" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description" class="control-label col-lg-3">Description</label>
                        <div class="col-lg-6">
                            <input class="form-control" id="cemail" type="text" name="description"
                                   value="{{ old('description') }}" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="curl" class="control-label col-lg-3">Sale</label>
                        <div class="col-lg-6">
                            <input class="form-control" id="curl" type="number" value="{{ old('sale') }}" name="sale">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="cprice" class="control-label col-lg-3">Price</label>
                        <div class="col-lg-6">
                            <input class="form-control" id="cprice" type="number" value="{{ old('price') }}"
                                   name="price" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="curl" class="control-label col-lg-3">Category</label>
                        <div class="col-lg-6">
                            <select name="categories[]" class="form-control select2_init" multiple>
                                @foreach($categories as $category)
                                    <option
                                        value="{{ $category->id }}" {{ in_array($category->id, old('categories', [])) ? 'selected' : '' }}>
                                        {{ $category->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="cimage" class="control-label col-lg-3">Image Path</label>
                        <div class="col-lg-6">
                            <input type="file" name="image_path"
                                   onchange="handleImageChange(this)" >
                                <img id="img-holder"  alt="your image"  class="img-responsive" width="200" height="200"  />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-3 col-lg-6">
                            <button class="btn btn-primary" type="submit">Save</button>
                            <button class="btn btn-default" type="button">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

