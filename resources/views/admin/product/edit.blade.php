@extends('admin.layout')
@section('content')
    <section class="panel">
        <header class="panel-heading">
            Edit Product
        </header>
        <div class="panel-body">
            @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            @if(session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="form">
                <form class="cmxform form-horizontal" id="commentForm" method="post" enctype="multipart/form-data"
                      action="{{ route('products.update', $product->id) }}" novalidate="novalidate">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="cname" class="control-label col-lg-3">Name </label>
                        <div class="col-lg-6">
                            <input class="form-control" id="cname" name="name" minlength="2" type="text"
                                   value="{{ old('name', $product->name) }}" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="cemail" class="control-label col-lg-3">Description</label>
                        <div class="col-lg-6">
                            <input class="form-control" id="cemail" type="text" name="description"
                                   value="{{ old('description', $product->description) }}" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="curl" class="control-label col-lg-3">Sale</label>
                        <div class="col-lg-6">
                            <input class="form-control" id="curl" type="number" name="sale"
                                   value="{{ old('sale', $product->sale) }}" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="cprice" class="control-label col-lg-3">Price</label>
                        <div class="col-lg-6">
                            <input class="form-control" id="cprice" type="number" name="price"
                                   value="{{ old('price', $product->price) }}" min="0">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="cimage" class="control-label col-lg-3">Image Path</label>
                        <div class="col-lg-6">
                            <input type="file" name="image_path"  onchange="handleImageChange(this)"  id="cimage" accept="image/*">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="curl" class="control-label col-lg-3">Category</label>
                        <div class="col-lg-6">
                            <select name="categories[]" class="form-control select2_init" multiple>
                                @foreach($categories as $category)
                                    <option
                                        {{ (collect(old('categories', $categoryOfProduct->pluck('id')))->contains($category->id)) ? 'selected' : '' }}
                                        value="{{ $category->id }}">{{ $category->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    @if(isset($product->image_path))
                        <div class="form-group">
                            <label class="control-label col-lg-3">Current Image</label>
                            <div class="col-lg-6">
                                <div class="img-holder">
                                    <img id="img-holder" src="{{ asset($product->image_path) }}" alt="Current Image" width="200" height="200"  >
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="form-group">
                            <label class="control-label col-lg-3"></label>
                            <div class="col-lg-6">
                                <div class="img-holder">
                                    <img id="img-holder"  alt="Current Image" class="img-responsive" >
                                </div>
                            </div>
                    @endif
                    <div class="form-group">
                        <div class="col-lg-offset-3 col-lg-6">
                            <button class="btn btn-primary" type="submit">Update</button>
                            <a href="{{ route('products.index') }}" class="btn btn-default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection




