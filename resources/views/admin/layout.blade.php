<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<head>
    <title>Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script type="application/x-javascript"> addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        } </script>
    <!-- bootstrap-css -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <!-- //bootstrap-css -->
    <!-- Custom CSS -->
    <link href="{{ asset('css/style.css') }}" rel='stylesheet' type='text/css'/>
    <link href="{{ asset('css/new.css') }}" rel='stylesheet' type='text/css'/>
    <link href="{{ asset('css/style-responsive.css') }}" rel="stylesheet"/>
    <link rel="stylesheet" href="{{ asset('css/morris.css') }}" type="text/css"/>
    <script src="{{ asset('js/jquery2.0.3.min.js') }}"></script>
    <script src="{{ asset('js/raphael-min.js') }}"></script>
    <script src="{{ asset('js/morris.js') }}"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/intl-tel-input@18.2.1/build/css/intlTelInput.css">
</head>
<body>
<section id="container">
    <!--header start-->
    <header class="header fixed-top clearfix">
        <!--logo start-->
        <div class="brand">
            <a href="{{route('dashboard')}}" class="logo">
                Page Admin
            </a>
            <div class="sidebar-toggle-box">
                <div class="fa fa-bars"></div>
            </div>
        </div>
        <!--logo end-->
        <div class="nav notify-row" id="top_menu">
            <!--  notification start -->
            <ul class="nav top-menu">
                <!-- settings start -->

                <!-- settings end -->
                <!-- inbox dropdown start-->

                <div class="top-nav clearfix">
                    <!--search & user info start-->
                    <ul class="nav pull-right top-menu">
                        <li>
                            <input type="text" class="form-control search" placeholder=" Search">
                        </li>
                        <!-- user login dropdown start-->
                        <li class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <span class="username">{{ Auth::user()->name }}</span>
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu extended logout">
                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        <i class="fa fa-key"></i> Log Out
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                          style="display: none;">
                                        @csrf
                                    </form>
                                </li>

                            </ul>
                        </li>
                        <!-- user login dropdown end -->

                    </ul>
                    <!--search & user info end-->
                </div>
    </header>
    <!--header end-->
    <!--sidebar start-->
    <aside>
        <div id="sidebar" class="nav-collapse">
            <!-- sidebar menu start-->
            <div class="leftside-navigation">
                <ul class="sidebar-menu" id="nav-accordion">
                    <li>
                        <a class="{{ request()->routeIs('dashboard') ? 'active' : '' }}" class="list" id="home"
                           href="{{ route('dashboard') }}">
                            <i class="fa fa-dashboard"></i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    @can('show-product')
                        <li class="sub-menu ">
                            <a class="list {{ request()->is('products*') ? 'active' : '' }}" href="javascript:">
                                <i class="fa fa-th"></i>
                                <span>Product</span>
                            </a>
                            <ul class="sub">
                                @can('show-product')
                                    <li class="{{ request()->routeIs('products.index') ? 'active' : '' }}">
                                        <a class="list" id="list-product-link" href="{{ route('products.index') }}">List
                                            Product</a>
                                    </li>
                                @endcan
                                @can('create-product')
                                    <li class="{{ request()->routeIs('products.create') ? 'active' : '' }}">
                                        <a class="list" href="{{ route('products.create') }}">Create Product</a>
                                    </li>
                                @endcan
                            </ul>
                        </li>
                    @endcan
                    @can('show-category')
                        <li class="sub-menu">
                            <a class="list {{ request()->is('categories*') ? 'active' : '' }}" href="javascript:">
                                <i class="fa fa-tasks"></i>
                                <span>Category</span>
                            </a>
                            <ul class="sub">
                                @can('show-category')
                                    <li class="{{ request()->routeIs('categories.index') ? 'active' : '' }}"><a
                                            href="{{ route('categories.index') }}">List Category</a></li>
                                @endcan
                                @can('create-category')
                                    <li class="{{ request()->routeIs('categories.create') ? 'active' : '' }}"><a
                                            href="{{ route('categories.create') }}">Create Category</a></li>
                                @endcan
                            </ul>
                        </li>
                    @endcan
                    @canManageRoles
                    <li class="sub-menu">
                        <a class="list {{ request()->is('roles*') ? 'active' : '' }}" href="javascript:">
                            <i class="fa fa-envelope"></i>
                            <span>Role </span>
                        </a>
                        <ul class="sub">
                            <li class="{{ request()->routeIs('roles.index') ? 'active' : '' }}"><a
                                    href="{{ route('roles.index') }}">List Role</a></li>
                            <li class="{{ request()->routeIs('roles.create') ? 'active' : '' }}"><a
                                    href="{{ route('roles.create') }}">Create Role </a></li>
                        </ul>
                    </li>
                    @endcanManageRoles
                    @can('show-user')
                        <li class="sub-menu">
                            <a class="list {{ request()->is('users*') ? 'active' : '' }}" href="javascript:">
                                <i class=" fa fa-bar-chart-o"></i>
                                <span>User</span>
                            </a>
                            <ul class="sub">
                                @can('show-user')
                                    <li class="{{ request()->routeIs('users.create') ? 'active' : '' }}"><a
                                            href="{{ route('users.create') }}">Create User </a></li>
                                @endcan
                                @can('create-user')
                                    <li class="{{ request()->routeIs('users.index') ? 'active' : '' }}"><a
                                            href="{{ route('users.index') }}">List User </a></li>
                                @endcan
                            </ul>
                        </li>
                    @endcan
                </ul>
            </div>
            <!-- sidebar menu end-->
        </div>
    </aside>
    <!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            @yield('content')

            <!-- footer -->
            <div class="footer">
                <div class="wthree-copyright">
                    <p> © 2023/12</p>
                </div>
            </div>
            <!-- / footer -->
        </section>
        <!--main content end-->
    </section>
</section>
    <script src="{{ asset('js/bootstrap.js') }}"></script>
    <script src="{{ asset('js/jquery.dcjqaccordion.2.7.js') }}"></script>
    <script src="{{ asset('js/scripts.js') }}"></script>
    <script src="{{ asset('js/jquery.slimscroll.js') }}"></script>
    <script src="{{ asset('js/jquery.nicescroll.js') }}"></script>
    <script src="{{ asset('js/jquery.scrollTo.js') }}"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" rel="stylesheet"/>
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="{{ asset('js/select.js') }}"></script>
    <script src="{{ asset('js/delete.js') }}"></script>
    <script src="{{ asset('js/update.js') }}"></script>
    <script src="{{ asset('js/image.js') }}"></script>
    <script src="{{ asset('js/intlTelInput.js') }}"></script>
    <script src="{{ asset('js/phone.js') }}"></script>
</body>
</html>






