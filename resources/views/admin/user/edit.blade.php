@extends('admin.layout')
@section('content')
    <section class="panel">
        <header class="panel-heading">
            Edit User
        </header>
        <div class="panel-body">
            @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            <!-- Add error message -->
            @if(session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
        <div class="form">
            <form class="cmxform form-horizontal" id="commentForm" method="post"
                  action="{{ route('users.update',['user_id' => $user->id]) }}" novalidate="novalidate">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="cname" class="control-label col-lg-3">Name (required)</label>
                    <div class="col-lg-6">
                        <input class="form-control" id="cname" name="name" minlength="2"
                               value="{{old('name',$user->name)}}" type="text" required="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="control-label col-lg-3">E-Mail (required)</label>
                    <div class="col-lg-6">
                        <input class="form-control" id="cemail" type="email" name="email"
                               value="{{ old('email',$user->email)}}" required="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="curl" class="control-label col-lg-3">Phone</label>
                    <div class="col-lg-6">
                        <input class="form-control phone-number" id="phone-update"  placeholder="" pattern="[0-9]{10}" type="tel" value="{{ old('phone',$user->phone) }}"
                               name="phone">
                        <input type="hidden" name="region_code" id="region_code" value="">
                        <input type="hidden" id="user-region-code-input" value="{{ $user->region_code ?? 'auto' }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="curl" class="control-label col-lg-3">Password</label>
                    <div class="col-lg-6">
                        <input class="form-control" id="curl" type="password" name="password">
                    </div>
                </div>
                <div class="form-group">
                    <label for="curl" class="control-label col-lg-3">Role</label>
                    <div class="col-lg-6">
                        <select name="roles[]" class="form-control select2_init" multiple>
                            @foreach($roles as $role)
                                <option
                                    {{ (collect(old('roles', $roleOfUser->pluck('id')))->contains($role->id)) ? 'selected' : '' }}
                                    value="{{ $role->id }}">{{ $role->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-offset-3 col-lg-6">
                        <button class="btn btn-primary" id="submit-btn"  onclick="updateRegionCodes(iti1)"  type="submit">Save</button>
                        <button class="btn btn-default" type="button">Cancel</button>
                    </div>
                </div>
            </form>
        </div>
        </div>
    </section>
@endsection
