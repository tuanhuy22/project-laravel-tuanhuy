@extends('admin.layout')
@section('content')

    <section class="wrapper">
        <div class="table-agile-info">
            <div class="panel panel-default">
                <div class="panel-heading">
                    List Users
                </div>
                <div class="row w3-res-tb">
                    <div class="col-sm-4">
                    </div>
                    <div class="col-sm-3 search-key">
                        <div class="input-group">
                            <form action="{{ route('users.index') }}" method="GET">
                                <input type="text" class="input-sm form-control" placeholder="Search" name="keyword"
                                       value="{{ $keyword }}">
                                <div class="input-group-btn">
                                    <button class="btn btn-sm btn-default" type="submit">Search</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    @if(session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif

                    <!-- Add error message -->
                    @if(session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <table class="table table-striped b-t b-light">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th style="width: 30px;"></th>
                            <th style="width: 30px;"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @if ($users->isEmpty())
                            <tr>
                                <td colspan="6">
                                    <div class="alert alert-info">
                                        No results found for keyword: {{ $keyword }}
                                    </div>
                                </td>
                            </tr>
                        @else
                            @foreach($users as $user)
                                <tr id="tr_{{$user->id}}">
                                    <td>{{$user->id}}</td>
                                    <td><span class="text-ellipsis">{{$user->name}}</span></td>
                                    <td><span class="text-ellipsis">{{$user->email}}</span></td>
                                    <td><span class="text-ellipsis">{{$user->e164_format}}</span></td>
                                    @can('update-user')
                                        <td>
                                            <a href="{{ route('users.edit', ['user_id' => $user->id]) }}"
                                               class="btn btn-primary">Edit</a>
                                        </td>
                                    @endcan
                                    @can('delete-user')
                                        <td>
                                            <a href="javascript:void(0)" onclick="deleteItem({{$user->id}}, 'users')"
                                               class="btn btn-danger delete-user">Delete</a>
                                        </td>
                                    @endcan
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
                <footer class="panel-footer">
                    <div class="row">
                        <div class="col-sm-7 text-right text-center-xs">
                            <ul class="pagination pagination-sm m-t-none m-b-none">
                                @if ($users->onFirstPage())
                                    <li class="disabled"><span>&laquo;</span></li>
                                @else
                                    <li><a href="{{ $users->previousPageUrl() }}" rel="prev">&laquo;</a></li>
                                @endif
                                @for ($i = 1; $i <= $users->lastPage(); $i++)
                                    <li class="{{ ($users->currentPage() == $i) ? 'active' : '' }}">
                                        <a href="{{ $users->url($i) }}">{{ $i }}</a>
                                    </li>
                                @endfor
                                @if ($users->hasMorePages())
                                    <li><a href="{{ $users->nextPageUrl() }}" rel="next">&raquo;</a></li>
                                @else
                                    <li class="disabled"><span>&raquo;</span></li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
    </section>

@endsection
