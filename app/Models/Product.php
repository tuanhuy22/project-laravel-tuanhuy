<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Intervention\Image\Facades\Image;

class Product extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'description','price','image_path','sale'];
    public function categories()
    {
        return $this->belongsToMany(Category::class, 'category_product', 'product_id', 'categories_id');
    }
    public function getFormattedPriceAttribute()
    {
        return '$' . number_format($this->price, 2);
    }
    public function setImagePathAttribute($value)
    {
        $this->attributes['image_path'] = $this->processAndSaveImage($value);
    }
    public function getImagePathAttribute($value)
    {
        return $value;
    }
    private function processAndSaveImage($image)
    {
        if (!$image instanceof UploadedFile) {
            return null;
        }
        $imageName = time() . '.' . $image->getClientOriginalExtension();
        $image->move(public_path('uploads/products'), $imageName);
        $resizedImage = Image::make(public_path('uploads/products/' . $imageName))
            ->resize(300, 200)
            ->save(public_path('uploads/products/resized_' . $imageName));
        return 'uploads/products/' . $imageName;
    }
}
