<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'description','parent_id'];

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }
    public function permissionsChildrent(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Permission::class,'parent_id');
    }
}
