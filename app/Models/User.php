<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'phone',
        'country_code',
        'region_code',
        'national_number',
        'e164_format'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = ['email_verified_at' => 'datetime'];
    protected static function boot()
    {
        parent::boot();

        static::created(function ($user) {
            if ($user->roles->isEmpty()) {
                // 5 is role id user
                $defaultRole = Role::find(5);
                if ($defaultRole) {
                    $user->roles()->attach($defaultRole);
                }
            }
        });
    }
    public function hasRole(string $role)
    {
        return $this->roles()->where('name', $role)->exists();
    }


    public function hasAnyRoles(array $roles)
    {
        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->hasRole($role)) {
                    return true;
                }
            }
        }
        return false;
    }
    public function roles()
    {
        return $this->belongsToMany(Role::class,'user_role','user_id','role_id');
    }
    public function  checkPermissionAccess( $permissionCheck )
    {
        $roles = auth()->user()->roles;
        foreach ($roles as $role) {
            $permissions = $role->permissions;
            if ($permissions->contains('name',$permissionCheck)) {
                return true;
            }
        }
        return false;
    }
}
