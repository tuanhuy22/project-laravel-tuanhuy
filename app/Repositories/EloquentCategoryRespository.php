<?php

namespace App\Repositories;

use App\Models\Category;
use App\Models\Role;

class EloquentCategoryRespository extends BaseRepository implements CategoryRespositoryInterface
{

    public function __construct(Category $category)
    {
        parent::__construct($category);
    }
    public function findWithCategoriesOrFail($category_id)
    {
        return $this->model->with('product')->findOrFail($category_id);
    }
}

