<?php

namespace App\Repositories;

use App\Models\Product;

class EloquentProductRespository extends BaseRepository implements ProductRespositoryInterface
{

    public function __construct(Product $product)
    {
        parent::__construct($product);
    }

    public function findWithCategoriesOrFail($product_id)
    {
        return $this->model->with('categories')->findOrFail($product_id);
    }

    public function search($minPrice, $maxPrice, $keyword, $categories)
    {
        $query = $this->model->query();

        if ($minPrice !== null) {
            $query->where('price', '>=', $minPrice);
        }
        if ($maxPrice !== null) {
            $query->where('price', '<=', $maxPrice);
        }
        if ($keyword) {
            $query->where('name', 'like', '%' . $keyword . '%');
        }
        if (!empty($categories)) {
            $query->whereHas('categories', function ($q) use ($categories) {
                $q->whereIn('id', $categories);
            });
        }
        return $query->paginate(10);
    }
}
