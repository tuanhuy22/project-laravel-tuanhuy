<?php

namespace App\Repositories;

use App\Models\Role;

class EloquentRoleRepository extends BaseRepository implements RoleRepositoryInterface
{

    public function __construct(Role $role)
    {
        parent::__construct($role);
    }

    public function findWithPermissionsOrFail($id)
    {
        return $this->model->with('permissions')->findOrFail($id);
    }
    public function search($keyword)
    {
        return $this->model->searchByNameAndDes($keyword)->paginate(10);
    }
}
