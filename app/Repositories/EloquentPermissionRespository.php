<?php

namespace App\Repositories;

use App\Models\Permission;
use App\Models\User;

class EloquentPermissionRespository extends BaseRepository implements PermissionRespositoryInterface
{

    public function __construct(Permission $permission)
    {
        parent::__construct($permission);
    }

    public function getPermissionsParent()
    {
        return $this->model->where('parent_id', 0)->get();
    }
}
