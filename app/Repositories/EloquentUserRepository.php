<?php

namespace App\Repositories;

use App\Models\User;

class EloquentUserRepository extends BaseRepository implements UserRepositoryInterface
{

    public function __construct(User $user)
    {
        parent::__construct($user);
    }

    public function findWithRolesOrFail($id)
    {
        return $this->model->with('roles')->findOrFail($id);
    }
    public function search($keyword)
    {
        return $this->model
            ->where('name', 'like', '%' . $keyword . '%')
            ->orWhere('email', 'like', '%' . $keyword . '%')
            ->orWhere('phone', 'like', '%' . $keyword . '%')
            ->paginate(10);
    }

}
