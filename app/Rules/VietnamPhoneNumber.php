<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class VietnamPhoneNumber implements Rule
{
    public function passes($attribute, $value)
    {
        return preg_match('/^0\d{9}$/', $value);
    }

    public function message()
    {
        return 'The phone must right format';
    }
}
