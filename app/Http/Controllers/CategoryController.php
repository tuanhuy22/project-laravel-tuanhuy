<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use App\Services\CategoryService;
use App\Services\ProductService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    private $categoryService;

    public function __construct(CategoryService $categoryService, ProductService $productService)
    {
        $this->categoryService = $categoryService;
        $this->productService = $productService;
    }

    public function index()
    {
        $categories = $this->categoryService->paginateCategory(10);
        return view('admin.category.index', compact('categories'));
    }

    public function create()
    {
        $products = $this->productService->getAllProducts();
        return view('admin.category.create',compact('products'));
    }

    public function store(CategoryRequest $request)
    {
        $dataCategory = $request->all();
        $this->categoryService->createCategory($dataCategory);
        return redirect()->route('categories.index')->with('success', 'Category created successfully');
    }

    public function edit($category_id)
    {
        try {
            $this->categoryService->getAndCheckCategoryById($category_id);
            $data = $this->categoryService->getDataCategroyEdit($category_id);
            return view('admin.category.edit', $data);
        } catch (ModelNotFoundException $e) {
            abort(404, 'Category not found');
        }
    }

    public function update($category_id, CategoryRequest $request)
    {
        try {
            $this->categoryService->getAndCheckCategoryById($category_id);
            $dataCategory = $request->all();
            $this->categoryService->updateCategory($category_id, $dataCategory);
            return redirect()->route('categories.index')->with('success', 'Category updated successfully.');
        } catch (ModelNotFoundException $e) {
            abort(404, 'Category not found');
        }
    }

    public function destroy($category_id)
    {
        try {
            $this->categoryService->getAndCheckCategoryById($category_id);
            $this->categoryService->deleteCategory($category_id);
            return response()->json(['success' => true, 'tr' => 'tr_' . $category_id]);
        } catch (ModelNotFoundException $e) {
            abort(404, 'Category not found');
        }
    }

}
