<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Models\Role;
use App\Models\User;
use App\Services\PermissionService;
use App\Services\ProductService;
use App\Services\CategoryServiceInterface;
use App\Services\RoleService;
use App\Services\UserService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    private $userService;
    private $roleService;

    public function __construct(UserService $userService, RoleService $roleService)
    {
        $this->userService = $userService;
        $this->roleService = $roleService;
    }

    public function index(Request $request)
    {
        $keyword = $request->input('keyword', '');
        $users = $this->userService->searchUsers($keyword);
        return view('admin.user.index', compact('users', 'keyword'));
    }

    public function create()
    {
        $roles = $this->roleService->getAllRoles();
        return view('admin.user.create', compact('roles'));
    }

    public function store(RegisterRequest $request)
    {
        $dataUser = $request->all();
        $this->userService->createUser($dataUser);
        return redirect()->route('users.index')->with('success', 'User created successfully. ');
    }

    public function edit($user_id)
    {
        try {
            $this->userService->getAndCheckUserById($user_id);
            $data = $this->userService->getUserDataForEdit($user_id);
            return view('admin.user.edit', $data);
        } catch (ModelNotFoundException $e) {
            abort(404, 'User not found');
        }
    }

    public function update(UserUpdateRequest $request, $user_id)
    {
        try {
            $this->userService->getAndCheckUserById($user_id);
            $dataUser = $request->all();
            $this->userService->updateUser($user_id, $dataUser);
            return redirect()->route('users.index')->with('success', 'User updated successfully.');
        } catch (ModelNotFoundException $e) {
            abort(404, 'User not found');
        }
    }

    public function destroy($user_id)
    {
        try {
            $this->userService->getAndCheckUserById($user_id);
            $this->userService->deleteUser($user_id);
            return response()->json(['success' => true, 'tr' => 'tr_' . $user_id]);
        } catch (ModelNotFoundException $e) {
            abort(404, 'User not found');
        }
    }

}

