<?php

namespace App\Http\Controllers;

use App\Http\Requests\RoleRequest;
use App\Models\Permission;
use App\Models\Role;
use App\Services\PermissionService;
use App\Services\RoleService;
use App\Services\RoleServiceInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class RoleController extends Controller
{
    private $roleService;
    private $permissionService;

    public function __construct(RoleService $roleService, PermissionService $permissionService)
    {
        $this->roleService = $roleService;
        $this->permissionService = $permissionService;
    }

    public function index(Request $request)
    {
        $keyword = $request->input('keyword', '');
        $roles = $this->roleService->searchRoles($keyword);
        return view('admin.roles.index', compact('roles','keyword'));
    }
    public function create()
    {
        $permissions = $this->permissionService->getPermissionsParent();
        return view('admin.roles.create', compact('permissions'));
    }

    public function store(RoleRequest $request)
    {
        $dataRole = $request->all();
        $this->roleService->createRole($dataRole);
        return redirect()->route('roles.index')->with('success', 'Role created successfully.');
    }

    public function edit($role_id)
    {
        try {
            $this->roleService->getAndCheckRoleById($role_id);
            $data = $this->roleService->getRoleDataForEdit($role_id);
            return view('admin.roles.edit', $data);
        } catch (ModelNotFoundException $e) {
            abort(404, 'Role not found');
        }
    }

    public function update(RoleRequest $request, $role_id)
    {
        try {
            $this->roleService->getAndCheckRoleById($role_id);
            $dataRole = $request->all();
            $this->roleService->updateRole($role_id, $dataRole);
            return redirect()->route('roles.index')->with('success', 'Role updated successfully.');
        } catch (ModelNotFoundException $e) {
            abort(404, 'Role not found');
        }
    }

    public function destroy($role_id)
    {
        try {
            $this->roleService->getAndCheckRoleById($role_id);
            $this->roleService->deleteRole($role_id);
            return response()->json(['success' => true, 'tr' => 'tr_' . $role_id]);
        } catch (ModelNotFoundException $e) {
            abort(404, 'Role not found');
        }
    }

}
