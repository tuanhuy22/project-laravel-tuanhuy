<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Models\Category;
use App\Models\Product;
use App\Models\User;
use App\Services\CategoryService;
use App\Services\ProductService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class ProductController extends Controller
{
    private $productService;
    private $categoryService;

    public function __construct(ProductService $productService, CategoryService $categoryService)
    {
        $this->productService = $productService;
        $this->categoryService = $categoryService;
    }

    public function index(Request $request)
    {
        $minPrice = $request->input('min_price');
        $maxPrice = $request->input('max_price');
        $keyword = $request->input('keyword');
        $categories = $request->input('categories', []);
        $categoriesList = $this->categoryService->getAllCategory();
        $products = $this->productService->searchProducts($minPrice, $maxPrice, $keyword, $categories);
        return view('admin.product.index', compact('products', 'minPrice', 'maxPrice', 'keyword', 'categories','categoriesList'));
    }
    public function create()
    {
        $categories = $this->categoryService->getAllCategory();
        return view('admin.product.create', compact('categories'));
    }

    public function store(ProductRequest $request)
    {
        $dataProduct = $request->all();
        $this->productService->createProduct($dataProduct);
        return redirect()->route('products.index')->with('success', 'Product created successfully');
    }

    public function edit($product_id)
    {
        try {
            $this->productService->getAndCheckProductById($product_id);
            $data = $this->productService->getProductDataForEdit($product_id);
            return view('admin.product.edit', $data);
        } catch (ModelNotFoundException $e) {
            abort(404, 'Product not found');
        }
    }

    public function update(ProductRequest $request, $product_id)
    {
        try {
            $this->productService->getAndCheckProductById($product_id);
            $dataProduct = $request->all();
            $this->productService->updateProduct($product_id, $dataProduct);
            return redirect()->route('products.index')->with('success', 'Product updated successfully');
        } catch (ModelNotFoundException $e) {
            abort(404, 'Product not found');
        }
    }

    public function destroy($id)
    {
        try {
            $this->productService->getAndCheckProductById($id);
            $this->productService->deleteProduct($id);
            return response()->json(['success' => true, 'tr' => 'tr_' . $id]);
        } catch (ModelNotFoundException $e) {
            abort(404, 'Product not found');
        }
    }

}

