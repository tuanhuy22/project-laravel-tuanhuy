<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use App\Rules\VietnamPhoneNumber;
use Carbon\Carbon;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;

class RegisterController extends Controller
{

    protected $redirectTo = RouteServiceProvider::HOME;

    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showRegistrationForm()
    {
        return view('auth.register');
    }

    public function register(RegisterRequest $request)
    {
        $data = $request->all();
        $phoneNumberObject = $this->validateAndParsePhoneNumber($data['phone'], $request->get('region_code'));
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'region_code' => $request->get('region_code'),
            'country_code' => $phoneNumberObject->getCountryCode(),
            'national_number' => $phoneNumberObject->getNationalNumber(),
            'e164_format' => $this->formatPhoneNumber($phoneNumberObject),
        ]);
        $token = Str::random(64);
        DB::table('password_resets')->insert([
            'email' => $data['email'],
            'token' => $token,
            'created_at' => Carbon::now(),
        ]);
        Mail::send('email.createPassword', ['token' => $token], function ($message) use ($data) {
            $message->to($data['email']);
            $message->subject('Create Password');
        });
        return back()->with('message', 'We have e-mailed your password create link!');
    }

    private function validateAndParsePhoneNumber($phoneNumber, $regionCode)
    {
        $phoneNumberUtil = PhoneNumberUtil::getInstance();
        try {
            $parsedNumber = $phoneNumberUtil->parse($phoneNumber, $regionCode);
            return $parsedNumber;
        } catch (NumberParseException $e) {
            return null;
        }
    }

    private function formatPhoneNumber($phoneNumberObject)
    {
        $phoneNumberUtil = PhoneNumberUtil::getInstance();
        return $phoneNumberUtil->format($phoneNumberObject, PhoneNumberFormat::E164);
    }

    public function showCreatePasswordForm($token)
    {
        return view('auth.createPasswordLink', ['token' => $token]);
    }

    public function createPassword(Request $request)
    {
        $request->validate([
            'email' => 'required|email|exists:users',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required'
        ]);
        $updatePassword = DB::table('password_resets')->where([
            'email' => $request->email,
            'token' => $request->token
        ])->first();
        if (!$updatePassword) {
            return back()->with('error', 'Invalid token!');
        }
        $user = User::where('email', $request->email)->first();
        if (!$user) {
            return back()->with('error', 'User not found!');
        }
        $user->update(['password' => Hash::make($request->password)]);
        DB::table('password_resets')->where(['email' => $request->email])->delete();
        return redirect('/login')->with('message', 'Your password has been created!');
    }
}
