<?php

namespace App\Http\Requests;

use App\Rules\VietnamPhoneNumber;
use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' =>'required|min:3',
            'email' => 'required|email',
            'roles' => 'required',
            'phone' => ['required',  'valid_phone:' . $this->get('region_code')]
        ];
    }
    public function messages()
    {
        $regionCode = $this->get('region_code');
        return [
            'name.required' => 'The name field is required.',
            'name.min' => 'The name must be at least :min characters.',
            'email.required' => 'The email field is required.',
            'roles.required' => 'The roles field is required.',
            'email.email' => 'The email must be right format. ',
            'phone.required' => 'The phone field is required ',
            'phone.valid_phone' => "Phone has the right format for region code $regionCode"
        ];
    }
}
