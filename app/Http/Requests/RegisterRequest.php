<?php

namespace App\Http\Requests;

use App\Rules\VietnamPhoneNumber;
use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'phone' => ['required',  'valid_phone:' . $this->get('region_code')]
        ];
    }
    public function messages()
    {
        $regionCode = $this->get('region_code');
        return [
            'name.required' => 'The name field is required.',
            'email.required' => 'The price field is required.',
            'email.email' => 'The email must right format' ,
            'phone.required' => 'The phone field is required',
            'name.max' => 'The name must be less than :max characters.',
            'email.max' => 'The email must be less than :max characters.',
            'password.max' => 'The password must be less than :max characters.',
            'email.unique' => 'The email has already been taken.',
            'phone.valid_phone' => "Phone has the right format for region code $regionCode"
        ];
    }
}
