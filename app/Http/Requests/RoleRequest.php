<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:255',
            'description' => 'required|min:3|max:255'
            //
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'The name field is required',
            'name.min' => 'The name must be at least :min characters.',
            'description.required' => 'The description field is required',
            'description.min' => 'The description must be at least :min characters.',
            'name.max' =>'The name must be less than :max characters.',
            'description.max' =>'The description must be less than :max characters.'
        ];
    }
}
