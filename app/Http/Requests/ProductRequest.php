<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|min:2|max:255',
            'description' => 'required|min:2|max:255',
            'sale' => 'required|numeric|gte:0',
            'price' => 'required|numeric|gt:0',
            'categories' => 'required',
            'image_path' => 'image'
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'The name field is required.',
            'name.min' => 'The name must be at least :min characters.',
            'description.required' => 'The description field is required.',
            'sale.required' => 'The sale field is required',
            'price.required' => 'The price field is required.',
            'price.numeric' => 'The price must be a number.',
            'categories.required' => 'The categories field is required',
            'image_path.image' => ' Image must be right format',
            'price.gt' => 'The price must be a valid number format.',
            'sale.gte' => 'The sale must be greater than or equal to 0.',
            'sale.numeric' => 'Sale must be a number',
            'name.max' =>'The name must be less than :max characters.',
            'description.max' =>'The description must be less than :max characters.',
        ];
    }
}
