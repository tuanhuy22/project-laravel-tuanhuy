<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'name' => 'required|min:2',
            'products' => 'required'
        ];
    }
    public function messages()
    {
        return [
            'name.rerquired' => 'The name field is required.',
            'name.min' => 'The name must be at least :min characters.',
            'products.rerquired' => 'The products field is required. '
        ];
    }
}
