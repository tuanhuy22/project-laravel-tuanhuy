<?php

namespace App\Providers;

use App\Policies\CategoryPolicy;
use App\Policies\ProductPolicy;
use App\Policies\UserPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('create-user', [UserPolicy::class, 'create']);
        Gate::define('show-user', [UserPolicy::class, 'view']);
        Gate::define('update-user', [UserPolicy::class, 'update']);
        Gate::define('delete-user', [UserPolicy::class, 'delete']);

        Gate::define('create-product', [ProductPolicy::class, 'create']);
        Gate::define('show-product', [ProductPolicy::class, 'view']);
        Gate::define('update-product', [ProductPolicy::class, 'update']);
        Gate::define('delete-product', [ProductPolicy::class, 'delete']);

        Gate::define('create-category', [CategoryPolicy::class, 'create']);
        Gate::define('show-category', [CategoryPolicy::class, 'view']);
        Gate::define('update-category', [CategoryPolicy::class, 'update']);
        Gate::define('delete-category', [CategoryPolicy::class, 'delete']);

        Gate::define('manage-roles', function ($user) {
            return $user->hasRole('super-admin');
        });
    }
}
