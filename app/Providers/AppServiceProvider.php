<?php

namespace App\Providers;

use App\Repositories\EloquentProductRespository;
use App\Repositories\EloquentRoleRepository;
use App\Repositories\EloquentUserRepository;
use App\Repositories\EloquentCategoryRespository;
use App\Repositories\EloquentPermissionRespository;
use App\Repositories\CategoryRespositoryInterface;
use App\Repositories\PermissionRespositoryInterface;
use App\Repositories\ProductRespositoryInterface;
use App\Repositories\RoleRepositoryInterface;
use App\Repositories\UserRepositoryInterface;
use App\Services\CategoryService;
use App\Services\PermissionService;
use App\Services\PermissionServiceInterface;
use App\Services\ProductServiceInterface;
use App\Services\RoleService;
use App\Services\RoleServiceInterface;
use App\Services\ProductService;
use App\Services\CategoryServiceInterface;
use App\Services\UserService;
use App\Services\UserServiceInterface;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UserRepositoryInterface::class, EloquentUserRepository::class);
        $this->app->bind(RoleRepositoryInterface::class, EloquentRoleRepository::class);
        $this->app->bind(PermissionRespositoryInterface::class, EloquentPermissionRespository::class);
        $this->app->bind(ProductRespositoryInterface::class, EloquentProductRespository::class);
        $this->app->bind(CategoryRespositoryInterface::class, EloquentCategoryRespository::class);

        $this->app->bind(UserServiceInterface::class, UserService::class);
        $this->app->bind(CategoryServiceInterface::class, CategoryService::class);
        $this->app->bind(PermissionServiceInterface::class, PermissionService::class);
        $this->app->bind(ProductServiceInterface::class, ProductService::class);
        $this->app->bind(RoleServiceInterface::class, RoleService::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::if('canManageRoles', function () {
            return auth()->check() && auth()->user()->can('manage-roles');
        });
    }
}
