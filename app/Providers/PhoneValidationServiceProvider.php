<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use libphonenumber\PhoneNumberUtil;
use libphonenumber\NumberParseException;

class PhoneValidationServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Validator::extend('valid_phone', function ($attribute, $value, $parameters, $validator) {
            $regionCode = $parameters[0] ?? null;
            if (!$regionCode) {
                return false;
            }
            $phoneUtil = PhoneNumberUtil::getInstance();
            try {
                $phoneNumber = $phoneUtil->parse($value, $regionCode);
                return $phoneUtil->isValidNumber($phoneNumber);
            } catch (NumberParseException $e) {
                return false;
            }
        });
    }
}


