<?php

namespace App\Services;

use App\Repositories\EloquentPermissionRespository;
use App\Repositories\EloquentUserRepository;
use App\Repositories\UserRepositoryInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use libphonenumber\PhoneNumberUtil;

class UserService implements UserServiceInterface
{
    private $userRepository;

    public function __construct(EloquentUserRepository $userRepository, RoleService $roleService)
    {
        $this->userRepository = $userRepository;
        $this->roleService = $roleService;
    }

    public function getAllUsers()
    {
        return $this->userRepository->all();
    }

    public function paginateUsers($data)
    {
        return $this->userRepository->paginate($data);
    }

    public function searchUsers($keyword)
    {
        return $this->userRepository->search($keyword);
    }

    public function getAndCheckUserById($id)
    {
        $user = $this->userRepository->find($id);
        if (!$user) {
            throw new ModelNotFoundException;
        }
        return $user;
    }

    public function createUser(array $data)
    {
        try {

            DB::beginTransaction();
            $data['password'] = Hash::make($data['password']);
            $phoneNumberObject = $this->validateAndParsePhoneNumber($data['phone'], $data['region_code']);
            $data['country_code'] = $phoneNumberObject->getCountryCode();
            $data['national_number'] = $phoneNumberObject->getNationalNumber();
            $data['e164_format'] = $this->formatPhoneNumber($phoneNumberObject);
            $user = $this->userRepository->create($data);
            $user->roles()->attach($data['roles']);
            DB::commit();
            return $user;
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    private function validateAndParsePhoneNumber($phoneNumber, $regionCode)
    {
        $phoneNumberUtil = PhoneNumberUtil::getInstance();
        try {
            $parsedNumber = $phoneNumberUtil->parse($phoneNumber, $regionCode);
            return $parsedNumber;
        } catch (\libphonenumber\NumberParseException $e) {
            return null;
        }
    }

    private function formatPhoneNumber($phoneNumberObject)
    {
        $phoneNumberUtil = PhoneNumberUtil::getInstance();
        return $phoneNumberUtil->format($phoneNumberObject, \libphonenumber\PhoneNumberFormat::E164);
    }

    public function getUserDataForEdit($id)
    {
        $user = $this->userRepository->findWithRolesOrFail($id);
        $roles = $this->roleService->getAllRoles();
        $roleOfUser = $user->roles;
        return compact('user', 'roles', 'roleOfUser');
    }

    public function updateUser($id, array $data)
    {
        try {
            DB::beginTransaction();
            $user = $this->userRepository->find($id);
            if (isset($data['password'])) {
                $data['password'] = Hash::make($data['password']);
            } else {
                unset($data['password']);
            }
            $user->roles()->sync($data['roles']);
            $phoneNumberObject = $this->validateAndParsePhoneNumber($data['phone'], $data['region_code']);
            $data['country_code'] = $phoneNumberObject->getCountryCode();
            $data['national_number'] = $phoneNumberObject->getNationalNumber();
            $data['e164_format'] = $this->formatPhoneNumber($phoneNumberObject);
            $this->userRepository->update($id, $data);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public function deleteUser($id)
    {
        try {
            DB::beginTransaction();
            $user = $this->userRepository->find($id);
            if (!$user) {
                return false;
            }
            $user->roles()->detach();
            $this->userRepository->delete($id);
            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

}

