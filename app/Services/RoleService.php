<?php

namespace App\Services;

use App\Repositories\EloquentCategoryRespository;
use App\Repositories\EloquentRoleRepository;
use App\Repositories\EloquentPermissionRespository;
use App\Repositories\CategoryRespositoryInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;

class RoleService implements RoleServiceInterface
{
    private $roleRepository;

    public function __construct(EloquentRoleRepository $roleRepository, PermissionService $permissionService)
    {
        $this->roleRepository = $roleRepository;
        $this->permissionService = $permissionService;
    }

    public function getAllRoles()
    {
        return $this->roleRepository->all();
    }
    public function getPaginateRoles($data)
    {
        return $this->roleRepository->paginate($data);
    }
    public function searchRoles($keyword)
    {
        if (!empty($keyword)) {
            return $this->roleRepository->search($keyword);
        } else {
            return $this->roleRepository->paginate(10);
        }
    }

    public function getAndCheckRoleById($id)
    {
        $role = $this->roleRepository->find($id);
        if (!$role) {
            throw new ModelNotFoundException;
        }
        return $role;
    }

    public function createRole(array $data)
    {
        $role = $this->roleRepository->create($data);
        $role->permissions()->attach($data['permission_id']);
    }
    public function getRoleDataForEdit($id)
    {
        $role = $this->roleRepository->findWithPermissionsOrFail($id);
        $permissionsParent = $this->permissionService->getPermissionsParent();
        $permissionChecked = $role->permissions;
        return compact('permissionsParent', 'role', 'permissionChecked');
    }
    public function updateRole($id, array $data)
    {
        try {
            DB::beginTransaction();
            $role = $this->roleRepository->find($id);
            $role->permissions()->sync($data['permission_id']);
            $this->roleRepository->update($id, $data);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public function deleteRole($id)
    {
        try {
            DB::beginTransaction();
            $roles = $this->roleRepository->find($id);
            $roles->permissions()->detach();
            $this->roleRepository->delete($id);
            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }
}

