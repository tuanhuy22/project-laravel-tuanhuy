<?php

namespace App\Services;

use App\Repositories\EloquentCategoryRespository;
use App\Repositories\EloquentRoleRepository;
use App\Repositories\EloquentPermissionRespository;
use App\Repositories\CategoryRespositoryInterface;
use App\Repositories\PermissionRespositoryInterface;
use Illuminate\Support\Facades\DB;

class PermissionService implements PermissionServiceInterface
{
    private $permissionRespository;

    public function __construct(EloquentPermissionRespository $permissionRespository)
    {
        $this->permissionRespository = $permissionRespository;
    }

    public function getAllPermissions()
    {
        return $this->permissionRespository->all();
    }

    public function getPermissionById($id)
    {
        return $this->permissionRespository->find($id);
    }

    public function getPermissionsParent()
    {
        return $this->permissionRespository->getPermissionsParent();
    }
}

