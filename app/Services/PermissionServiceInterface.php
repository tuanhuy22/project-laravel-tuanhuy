<?php

namespace App\Services;

interface PermissionServiceInterface
{
    public function getAllPermissions();

    public function getPermissionById($id);

}

