<?php

namespace App\Services;

interface RoleServiceInterface
{
    public function getAllRoles();

    public function getAndCheckRoleById($id);

    public function createRole(array $data);

    public function updateRole($id, array $data);

    public function deleteRole($id);
}

