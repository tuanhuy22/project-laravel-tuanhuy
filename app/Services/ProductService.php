<?php

namespace App\Services;

use App\Repositories\EloquentCategoryRespository;
use App\Repositories\EloquentProductRespository;
use App\Repositories\EloquentPermissionRespository;
use App\Repositories\UserRepositoryInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class ProductService implements ProductServiceInterface
{
    private $productRepository;

    public function __construct(EloquentProductRespository $productRepository, CategoryService $categoryService)
    {
        $this->productRepository = $productRepository;
        $this->categoryService = $categoryService;
    }

    public function getAllProducts()
    {
        return $this->productRepository->all();
    }
    public function searchProducts($minPrice, $maxPrice, $keyword, $categories)
    {
        return $this->productRepository->search($minPrice, $maxPrice, $keyword, $categories);
    }
    public function getAndCheckProductById($id)
    {
        $product = $this->productRepository->find($id);
        if (!$product) {
            throw new ModelNotFoundException;
        }
        return $product;
    }

    public function createProduct($requestData)
    {
        try {
            $dataToCreate = [
                'name' => $requestData['name'],
                'description' => $requestData['description'],
                'sale' => $requestData['sale'],
                'price' => $requestData['price'],
            ];
            if (isset($requestData['image_path'])) {
                $dataToCreate['image_path'] = $requestData['image_path'];
            }
            $product = $this->productRepository->create($dataToCreate);
            $this->attachCategories($product, $requestData['categories']);
            return $product;
        } catch (\Exception $e) {
            throw $e;
        }
    }
    private function attachCategories($product, array $categories)
    {
        $product->categories()->attach($categories);
    }
    public function getProductDataForEdit($product_id)
    {
        $product = $this->productRepository->findWithCategoriesOrFail($product_id);
        $categories = $this->categoryService->getAllCategory();
        $categoryOfProduct = $product->categories;
        return compact('product', 'categoryOfProduct', 'categories');
    }

    public function updateProduct($productId, $requestData)
    {
        try {
            $product = $this->productRepository->find($productId);
            $dataToUpdate = [
                'name' => $requestData['name'],
                'description' => $requestData['description'],
                'sale' => $requestData['sale'],
                'price' => $requestData['price'],
            ];
            if (isset($requestData['categories'])) {
                $product->categories()->sync($requestData['categories']);
            }
            if (isset($requestData['image_path']) && $requestData['image_path'] instanceof UploadedFile) {
                if ($product->image_path && file_exists(public_path($product->image_path))) {
                    unlink(public_path($product->image_path));
                }
                $dataToUpdate['image_path'] = $requestData['image_path'];
            }
            $this->productRepository->update($productId, $dataToUpdate);
            return $product;
        } catch (\Exception $e) {
            throw $e;
        }
    }
    public function deleteProduct($productId)
    {
        try {
            DB::beginTransaction();
            $product = $this->productRepository->find($productId);
            if (!$product) {
                return false;
            }
            if ($product->image_path && File::exists(public_path($product->image_path))) {
                File::delete(public_path($product->image_path));
            }
            $product->categories()->detach();
            $this->productRepository->delete($productId);
            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

}


