<?php

namespace App\Services;

use App\Repositories\EloquentCategoryRespository;
use App\Repositories\EloquentPermissionRespository;
use App\Repositories\EloquentProductRespository;
use App\Repositories\UserRepositoryInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class CategoryService implements CategoryServiceInterface
{
    private $categoryRepository;
    private $productRespository;

    public function __construct(EloquentCategoryRespository $categoryRepository, EloquentProductRespository $productRespository)
    {
        $this->categoryRepository = $categoryRepository;
        $this->productRepository = $productRespository;
    }

    public function getAllCategory()
    {
        return $this->categoryRepository->all();
    }

    public function paginateCategory($data)
    {
        return $this->categoryRepository->paginate($data);
    }

    public function getAndCheckCategoryById($id)
    {
        $category = $this->categoryRepository->find($id);
        if (!$category) {
            throw new ModelNotFoundException;
        }
        return $category;
    }

    public function createCategory(array $data)
    {
        try {
            DB::beginTransaction();
            $category = $this->categoryRepository->create([
                'name' => $data['name'],
                'parent_id' => 0
            ]);
            $this->attachProducts($category, $data['products']);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }
    private function attachProducts($category, array $products)
    {
        $category->product()->attach($products);
    }
    public function getDataCategroyEdit($id)
    {
       $category = $this->categoryRepository->findWithCategoriesOrFail($id);
       $products = $this->productRepository->all();
       $productOfCategory = $category->product;
       return compact('category','products','productOfCategory');
    }


    public function updateCategory($id, array $data)
    {
        try {
            DB::beginTransaction();
            $category = $this->categoryRepository->find($id);
            $this->categoryRepository->update($id, $data);
            $category->product()->sync($data['products']);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public function deleteCategory($id)
    {
        try {
            DB::beginTransaction();
            $category = $this->categoryRepository->find($id);
            $category->product()->detach();
            $this->categoryRepository->delete($id);
            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

}

