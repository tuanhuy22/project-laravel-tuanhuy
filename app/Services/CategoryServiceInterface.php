<?php

namespace App\Services;

interface CategoryServiceInterface
{
    public function getAllCategory();
    public function getAndCheckCategoryById($id);
    public function createCategory(array $data);
    public function updateCategory($id, array $data);
    public function deleteCategory($id);
}

