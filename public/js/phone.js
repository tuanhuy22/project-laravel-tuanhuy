const createIntlTelInput = (input, initialCountry) => {
    return window.intlTelInput(input, {
        utilsScript: "https://cdn.jsdelivr.net/npm/intl-tel-input@18.2.1/build/js/utils.js",
        initialCountry: initialCountry,
    });
};
const userRegionCodeInput = document.getElementById('user-region-code-input');
const userRegionCode = userRegionCodeInput ? userRegionCodeInput.value : 'auto';
const input1 = document.querySelector("#phone-update");
const iti1 = input1 ? createIntlTelInput(input1, userRegionCode) : null;
const input2 = document.querySelector("#phone");
const iti2 = input2 ? createIntlTelInput(input2, "vn") : null;
const input3 = document.querySelector("#phone-register");
const iti3 = input3 ? createIntlTelInput(input3, "vn") : null;
const updateRegionCodes = (iti) => {
    if (iti) {
        document.getElementById("region_code").value = iti.getSelectedCountryData().iso2;
    }
};
