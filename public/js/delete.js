function deleteItem(id, itemType) {
    const successMessage = 'Deleted!';
    const successIcon = 'success';
    Swal.fire({
        title: "Are you sure?",
        text: "You won't be able to revert this!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes, delete it!"
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            new Promise((resolve, reject) => {
                $.ajax({
                    url: itemType + '/' + id,
                    type: 'DELETE',
                    success: function (result) {
                        $("#" + result['tr']).slideUp(50);
                        resolve();
                    },
                    error: function (xhr, status, error) {
                        reject(error);
                    }
                });
            }).then(() => {
                Swal.fire({
                    title: successMessage,
                    text: `${itemType} has been deleted.`,
                    icon: successIcon
                });
            }).catch((error) => {
                console.error(`Error deleting ${itemType}:`, error);
                Swal.fire({
                    title: "Error!",
                    text: `Failed to delete the ${itemType}. Please try again later.`,
                    icon: "error"
                });
            });
        }
    });
}
