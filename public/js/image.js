function handleImageChange(input) {
    var img_holder = document.getElementById('img-holder');
    if (input.files && input.files[0]) {
        var imageURL = window.URL.createObjectURL(input.files[0]);
        img_holder.src = imageURL;
        img_holder.style.display = 'block';
    } else {
        img_holder.src = '';
        img_holder.style.display = 'none';
    }
}
