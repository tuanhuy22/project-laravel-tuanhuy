<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        $roles = [
            ['name' => 'super-admin', 'description' => 'Super Admin'],
            ['name' => 'admin', 'description' => 'Admin'],
            ['name' => 'employee', 'description' => 'employee'],
            ['name' => 'manager', 'description' => 'manager'],
            ['name' => 'user', 'description' => 'user'],
        ];

        foreach($roles as $role) {
            Role::updateOrCreate($role);
        }
        $superAdmin = User::whereEmail('admin@gmail.com')->first();

        if(!$superAdmin) {
            $superAdmin = User::factory()->create(['name'=>'admin','email' => 'admin@gmail.com','password'=>Hash::make('12345678')]);
        }

        $permissions = [
            ['name' => 'user', 'description' => 'User','parent_id'=>0 ],
            ['name' => 'create-user', 'description' => 'Create user','parent_id'=>1 ],
            ['name' => 'update-user', 'description' => 'Update user','parent_id'=>1],
            ['name' => 'show-user', 'description' => 'Show user','parent_id'=>1],
            ['name' => 'delete-user', 'description' => 'Delete user','parent_id'=>1],

            ['name' => 'role', 'description' => ' Role','parent_id'=>0],
            ['name' => 'create-role', 'description' => 'Create Role','parent_id'=>6],
            ['name' => 'update-role', 'description' => 'Update Role','parent_id'=>6],
            ['name' => 'show-role', 'description' => 'Show Role','parent_id'=>6],
            ['name' => 'delete-role', 'description' => 'Delete Role','parent_id'=>6],

            ['name' => 'category', 'description' => ' category','parent_id'=>0],
            ['name' => 'create-category', 'description' => 'Create category','parent_id'=>11],
            ['name' => 'update-category', 'description' => 'Update category','parent_id'=>11],
            ['name' => 'show-category', 'description' => 'Show category','parent_id'=>11],
            ['name' => 'delete-category', 'description' => 'Delete category','parent_id'=>11],

            ['name' => 'product', 'description' => 'product','parent_id'=>0],
            ['name' => 'create-product', 'description' => 'Create product','parent_id'=>16],
            ['name' => 'update-product', 'description' => 'Update product','parent_id'=>16],
            ['name' => 'show-product', 'description' => 'Show product','parent_id'=>16],
            ['name' => 'delete-product', 'description' => 'Delete product','parent_id'=>16],

            ['name' => 'coupon', 'description' => 'coupon','parent_id'=>21],
            ['name' => 'create-coupon', 'description' => 'Create coupon','parent_id'=>21],
            ['name' => 'update-coupon', 'description' => 'Update coupon','parent_id'=>21],
            ['name' => 'show-coupon', 'description' => 'Show coupon','parent_id'=>21],
            ['name' => 'delete-coupon', 'description' => 'Delete coupon','parent_id'=>21],

            ['name' => 'list-order', 'description' => 'list order','parent_id'=>0],
            ['name' => 'update-order-status', 'description' => 'Update order status','parent_id'=>0],
        ];
        foreach($permissions as $item){
            Permission::updateOrCreate($item);
        }
    }
}
