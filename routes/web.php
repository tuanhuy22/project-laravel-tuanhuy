<?php

use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('auth')->group(function () {
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');

    // Route::resource('roles', RoleController::class);
    Route::prefix('roles')->middleware('roles:super-admin')->controller(RoleController::Class)->name('roles.')->group(
        function () {
            Route::get('/', 'index')->name('index');
            Route::post('/', 'store')->name('store');
            Route::get('/create', 'create')->name('create');
            Route::get('/{role_id}', 'show')->name('show');
            Route::put('/{role_id}', 'update')->name('update');
            Route::delete('/{role_id}', 'destroy')->name('destroy');
            Route::get('/edit/{role_id}', 'edit')->name('edit');
        }
    );
    // Route::resource('users', UserController::class);
    Route::prefix('users')->controller(UserController::Class)->name('users.')->group(function () {
        Route::get('/', 'index')->name('index')->middleware('can:show-user');
        Route::post('/', 'store')->name('store')->middleware('can:create-user');
        Route::get('/create', 'create')->name('create')->middleware('can:create-user');
        Route::get('/{user_id}', 'show')->name('show')->middleware('can:show-user');
        Route::put('/{user_id}', 'update')->name('update')->middleware('can:update-user');
        Route::delete('/{user_id}', 'destroy')->name('destroy')->middleware('can:delete-user');
        Route::get('/edit/{user_id}', 'edit')->name('edit')->middleware('can:update-user');
    });
    // Route::resource('categories', CategoryController::class);
    Route::prefix('categories')->controller(CategoryController::Class)->name('categories.')->group(function () {
        Route::get('/', 'index')->name('index')->middleware('can:show-category');
        Route::post('/', 'store')->name('store')->middleware('can:create-category');
        Route::get('/create', 'create')->name('create')->middleware('can:create-category');
        Route::get('/{category_id}', 'show')->name('show')->middleware('can:show-category');
        Route::put('/{category_id}', 'update')->name('update')->middleware('can:update-category');
        Route::delete('/{category_id}', 'destroy')->name('destroy')->middleware('can:delete-category');
        Route::get('/edit/{category_id}', 'edit')->name('edit')->middleware('can:update-category');
    });
    // Route::resource('products', ProductController::class);
    Route::prefix('products')->controller(ProductController::Class)->name('products.')->group(function () {
        Route::get('/', 'index')->name('index')->middleware('can:show-product');
        Route::post('/', 'store')->name('store')->middleware('can:create-product');
        Route::get('/create', 'create')->name('create')->middleware('can:create-product');
        Route::get('/{product_id}', 'show')->name('show')->middleware('can:show-product');
        Route::put('/{product_id}', 'update')->name('update')->middleware('can:update-product');
        Route::delete('/{product_id}', 'destroy')->name('destroy')->middleware('can:delete-product');
        Route::get('/edit/{product_id}', 'edit')->name('edit')->middleware('can:update-product');
    });
    Route::get('/home', [HomeController::class, 'index'])->name('home');
    Route::post('/logout', [LoginController::class, 'logout'])->name('logout');
});
Auth::routes();
    Route::controller(ForgotPasswordController::class)->group(function () {
        Route::get('forget-password', 'showForgetPasswordForm')->name('forget.password.get');
        Route::post('forget-password', 'sendMail')->name('forget.password.post');
        Route::get('reset-password/{token}', 'showResetPasswordForm')->name('reset.password.get');
        Route::post('reset-password', 'resetPassword')->name('reset.password.post');
    });

    Route::controller(RegisterController::class)->group(function () {
        Route::get('create-password/{token}','showCreatePasswordForm')->name('create.password.get');
        Route::post('create-password','createPassword')->name('create.password.post');
    });
