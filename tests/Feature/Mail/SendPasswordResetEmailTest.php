<?php

namespace Tests\Feature\Mail;

use App\Jobs\SendPasswordResetEmail;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Queue;
use Tests\TestCase;

class SendPasswordResetEmailTest extends TestCase
{
    use RefreshDatabase;
    /** @test */
    public function it_sends_password_reset_email_job()
    {
        Queue::fake();
        $email = 'user@example.com';
        $token = 'random_token';
        Bus::dispatch(new SendPasswordResetEmail($email, $token));
        Queue::assertPushed(SendPasswordResetEmail::class, function ($job) use ($email, $token) {
            return $job->email === $email && $job->token === $token;
        });
        Queue::assertPushed(SendPasswordResetEmail::class, 1);
        Mail::assertSent('email.forgetPassword', function ($mail) use ($email, $token) {
            return $mail->to[0]['address'] === $email &&
                $mail->viewData['token'] === $token &&
                $mail->subject === 'Reset Password';
        });
    }
}

