<?php

namespace Tests\Feature\Mail;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\Concerns\InteractsWithDatabase;
use Tests\TestCase;

class ForgotPasswordTest extends TestCase
{
    /** @test */
    public function it_shows_forgot_password_form()
    {
        $response = $this->get('/forget-password');
        $response->assertStatus(200)->assertViewIs('auth.forgetPassword');
    }

    /** @test */
    public function it_sends_password_reset_email()
    {
        $user = User::factory()->create();
        $response = $this->post('/forget-password', ['email' => $user->email]);
        $response->assertRedirect();
        $this->assertDatabaseHas('password_resets', ['email' => $user->email]);
    }
}

