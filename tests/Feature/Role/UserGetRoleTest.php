<?php

namespace Tests\Feature\Role;

use App\Models\Product;
use App\Models\Role;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;

class UserGetRoleTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_index_displays_role_list()
    {
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $role = Role::find(1);
        $response = $this->get(route('roles.index'));
        $response->assertStatus(200);
        $response->assertViewIs('admin.roles.index');
        $response->assertViewHas('roles', function ($roles) use ($role) {
            return $roles->contains($role);
        });
    }

    public function test_cant_index_displays_role_list_if_not_login()
    {
        $response = $this->get(route('roles.index'));
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }
}
