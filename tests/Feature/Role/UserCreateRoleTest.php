<?php

namespace Tests\Feature\Role;


use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;

class UserCreateRoleTest extends TestCase
{
    use WithFaker;

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_create_displays_create_form()
    {
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $response = $this->get(route('roles.create'));
        $response->assertStatus(200);
        $response->assertViewIs('admin.roles.create');
    }

    public function test_store_role()
    {
        $this->withoutExceptionHandling();
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $Data = [
            'name' => $this->faker->name,
            'description' => 'Role description',
            'permission_id' => [1, 2]
        ];
        $response = $this->post(route('roles.store'), $Data);
        $response->assertRedirect(route('roles.index'));
        $response->assertStatus(302);
        $response->assertSessionHas('success', 'Role created successfully.');
    }

    public function test_cant_store_role_if_not_login()
    {
        $roleData = [
            'name' => $this->faker->name,
            'description' => 'Role description',
            'permission_id' => [1, 2]
        ];
        $response = $this->post(route('roles.store'), $roleData);
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }

    public function test_cant_store_role_if_not_have_description()
    {
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $this->get(route('roles.create'))->assertStatus(200);
        $roleData = [
            'name' => 'New Role',
            'permission_id' => [1, 2]
        ];
        $response = $this->put(route('roles.store'), $roleData);
        $response->assertSee('The description field is required.');
    }
    public function test_cant_store_role_if_not_have_name()
    {
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $this->get(route('roles.create'))->assertStatus(200);
        $roleData = [
            'permission_id' => [1, 2]
        ];
        $response = $this->put(route('roles.store'), $roleData);
        $response->assertSee('The name field is required.');
    }
    public function test_cant_store_role_if_name_not_valid()
    {
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $this->get(route('roles.create'))->assertStatus(200);
        $roleData = [
             'name' => '1',
            'permission_id' => [1, 2]
        ];
        $response = $this->put(route('roles.store'), $roleData);
        $response->assertSee('The name must be at least 3 characters.');
    }
    public function test_cant_store_role_if_des_not_valid()
    {
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $this->get(route('roles.create'))->assertStatus(200);
        $roleData = [
             'description' => '1',
            'permission_id' => [1, 2]
        ];
        $response = $this->put(route('roles.store'), $roleData);
        $response->assertSee('The description must be at least 3 characters.');
    }


}
