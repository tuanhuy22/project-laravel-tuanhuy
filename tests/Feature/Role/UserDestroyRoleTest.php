<?php

namespace Tests\Feature\Role;

use App\Models\Product;
use App\Models\Role;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;

class UserDestroyRoleTest extends TestCase
{
    use withFaker;

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_can_destroy_role()
    {
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $role = Role::create([
            'name' => $this->faker->name,
            'description' => 'Test Role Description',
            'permission_id' => [1, 2],
        ]);
        $response = $this->delete(route('roles.destroy', ['role_id' => $role->id]));
        $response->assertStatus(200);
        $this->assertDeleted('roles', ['id' => $role->id]);
    }

    public function test_cant_destroy_role_if_not_login()
    {
        $role = Role::create([
            'name' => $this->faker->name,
            'description' => 'Test Role Description',
            'permission_id' => [1, 2],
        ]);
        $response = $this->delete(route('roles.destroy', ['role_id' => $role->id]));
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }
}
