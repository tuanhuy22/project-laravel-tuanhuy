<?php

namespace Tests\Feature\Role;

use App\Models\Product;
use App\Models\Role;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Str;
use Tests\TestCase;
use App\Models\User;

class UserUpdateRoleTest extends TestCase
{
    use WithFaker;

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_edit_displays_edit_form()
    {
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $role = Role::create([
            'name' => $this->faker->name,
            'description' => $this->faker->text,
            'permission_id' => [1, 2],
        ]);
        $response = $this->get(route('roles.edit', $role->id));
        $response->assertStatus(200);
        $response->assertViewIs('admin.roles.edit');
        $response->assertViewHas('role', $role);
    }

    public function test_update_role()
    {
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $role = Role::create([
            'name' => $this->faker->name,
            'description' => $this->faker->text,
            'permission_id' => [1, 2],
        ]);
        $updatedData = [
            'name' => $this->faker->name,
            'description' => $this->faker->text,
            'permission_id' => [1, 2]
        ];
        $response = $this->put(route('roles.update', $role->id), $updatedData);
        $response->assertRedirect(route('roles.index'));
    }

    public function test_cant_edit_displays_edit_form_if_not_login()
    {
        $role = Role::create([
            'name' => $this->faker->name,
            'description' => 'Test Role Description',
            'permission_id' => [1, 2],
        ]);
        $response = $this->get(route('roles.edit', $role->id));
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }

    public function test_cant_update_updates_role_if_not_login()
    {
        $role = Role::create([
            'name' => $this->faker->name,
            'description' => 'Test Role Description',
            'permission_id' => [1, 2],
        ]);
        $updatedData = [
            'description' => 'Test Role Description',
        ];
        $response = $this->put(route('roles.update', $role->id), $updatedData);
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }
    public function test_cant_update_role_if_name_not_valid()
    {
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $role = Role::create([
            'name' => $this->faker->name,
            'description' => $this->faker->text,
            'permission_id' => [1, 2],
        ]);
        $updatedData = [
            'name' => Str::random(300),
            'description' => $this->faker->text,
            'permission_id' => [1, 2]
        ];
        $response = $this->post(route('roles.update', $role->id), $updatedData);
        $response->assertSee('The name must be less than 255 characters.');
    }
    public function test_cant_update_role_if_des_not_valid()
    {
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $role = Role::create([
            'name' => $this->faker->name,
            'description' => $this->faker->text,
            'permission_id' => [1, 2],
        ]);
        $updatedData = [
            'name' => Str::random(300),
            'description' => Str::random(300),
            'permission_id' => [1, 2]
        ];
        $response = $this->post(route('roles.update', $role->id), $updatedData);
        $response->assertSee('The description must be less than 255 characters.');
    }
}
