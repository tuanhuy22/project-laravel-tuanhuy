<?php

namespace Tests\Feature\Product;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Str;
use Tests\TestCase;
use App\Models\User;

class UserUpdateProductTest extends TestCase
{
    use WithFaker;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_edit_displays_edit_form()
    {
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $product = Product::factory()->create();
        $response = $this->get(route('products.edit', $product->id));
        $response->assertStatus(200);
        $response->assertViewIs('admin.product.edit');
        $response->assertViewHas('product', $product);
    }
    public function test_update_product()
    {
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $product = Product::factory()->create();
        $updatedData = [
            'name' => 'Updated Product Name',
            'description' => 'Updated Product Description',
            'price' => 999.99,
            'sale' => 0,
            'categories' => [1]
        ];
        $response = $this->put(route('products.update', $product->id), $updatedData);
        $response->assertRedirect(route('products.index'));
    }
    public function test_cant_edit_displays_edit_form_if_not_login()
    {
        $product = Product::factory()->create();
        $response = $this->get(route('products.edit', $product->id));
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }
    public function test_cant_update_updates_product_if_not_login()
    {
        $product = Product::factory()->create();
        $updatedData = [
            'name' => 'Updated Product Name',
            'description' => 'Updated Product Description',
            'sale' => 0,
            'price' => 999.99,
        ];
        $response = $this->put(route('products.update', $product->id), $updatedData);
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }
    public function test_cant_update_product_if_name_not_valid()
    {
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $product = Product::factory()->create();
        $updatedData = [
            'name' => '1',
            'description' => 'Updated Product Description',
            'price' => 999.99,
            'sale' => 0,
            'categories' => [1]
        ];
        $response = $this->post(route('products.update', $product->id), $updatedData);
        $response->assertSee('The name must be at least 2 characters.');
    }
    public function test_cant_update_product_if_des_not_valid()
    {
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $product = Product::factory()->create();
        $updatedData = [
            'name' => $this->faker->name,
            'description' => Str::random(300),
            'price' => 999.99,
            'sale' => 0,
            'categories' => [1]
        ];
        $response = $this->post(route('products.update', $product->id), $updatedData);
        $response->assertSee('The description must be less than 255 characters.');
    }
    public function test_cant_update_product_if_price_not_valid()
    {
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $product = Product::factory()->create();
        $updatedData = [
            'name' => $this->faker->name,
            'description' => 'Updated Product Description',
            'price' => 5,
            'sale' => 0,
            'categories' => [1]
        ];
        $response = $this->post(route('products.update', $product->id), $updatedData);
        $response->assertSee('The price must be a valid number format.');
    }
    public function test_cant_update_product_if_sale_not_valid()
    {
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $product = Product::factory()->create();
        $updatedData = [
            'name' => $this->faker->name,
            'description' => 'Updated Product Description',
            'price' => 999.99,
            'sale' => -5,
            'categories' => [1]
        ];
        $response = $this->post(route('products.update', $product->id), $updatedData);
        $response->assertSee('The sale must be greater than or equal to 0.');
    }
    public function test_cant_update_product_if_categories_not_valid()
    {
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $product = Product::factory()->create();
        $updatedData = [
            'name' => $this->faker->name,
            'description' => 'Updated Product Description',
            'price' => 999.99,
            'sale' => -5,
            'categories' => []
        ];
        $response = $this->post(route('products.update', $product->id), $updatedData);
        $response->assertSee('The categories field is required');
    }
    public function test_cant_update_product_if_image_not_valid()
    {
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $product = Product::factory()->create();
        $updatedData = [
            'name' => $this->faker->name,
            'description' => 'Updated Product Description',
            'price' => 999.99,
            'sale' => -5,
            'categories' => [1],
            'image_path' => '1'
        ];
        $response = $this->post(route('products.update', $product->id), $updatedData);
        $response->assertSee('Image must be right format');
    }
}
