<?php

namespace Tests\Feature\Product;


use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Str;
use Tests\TestCase;
use App\Models\User;

class UserCreateProductTest extends TestCase
{
    use WithFaker;

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_create_displays_create_form()
    {
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $response = $this->get(route('products.create'));
        $response->assertStatus(200);
        $response->assertViewIs('admin.product.create');
    }

    public function test_store_product()
    {
        $this->withoutExceptionHandling();
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $productData = [
            'name' => $this->faker->name,
            'description' => 'Product description',
             'sale' => 0,
            'price' => 50.99,
            'categories' => [1],
        ];
        $response = $this->post(route('products.store'), $productData);
        $response->assertRedirect(route('products.index'));
        $response->assertStatus(302);
        $response->assertSessionHas('success', 'Product created successfully');
    }

    public function test_cant_store_product_if_not_login()
    {
        $productData = [
            'name' => 'New Product',
            'description' => 'Product description',
            'sale' => 0,
            'price' => 50.99,
            'categories' => [1],
            'image_path' => null
        ];
        $response = $this->post(route('products.store'), $productData);
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }

    public function test_cant_store_product_if_not_have_price()
    {
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $this->get(route('products.create'))->assertStatus(200);
        $productData = [
            'name' => 'New Product',
            'description' => 'Product description',
            'sale' => 0,
        ];
        $response = $this->put(route('products.store'), $productData);
        $response->assertSee('The price field is required.');
    }
    public function test_cant_store_product_if_not_have_name()
    {
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $this->get(route('products.create'))->assertStatus(200);
        $productData = [
            'description' => 'Product description',
            'sale' => 0,
        ];
        $response = $this->put(route('products.store'), $productData);
        $response->assertSee('The name field is required.');
    }
    public function test_cant_store_product_if_not_have_des()
    {
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $this->get(route('products.create'))->assertStatus(200);
        $productData = [
            'name' => 'New Product',
            'sale' => 0,
            'price' => 50.99,
            'categories' => [1],
            'image_path' => null
        ];
        $response = $this->put(route('products.store'), $productData);
        $response->assertSee('The description field is required.');
    }
    public function test_cant_store_product_if_not_have_sale()
    {
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $this->get(route('products.create'))->assertStatus(200);
        $productData = [
            'name' => 'New Product',
            'price' => 50.99,
            'categories' => [1],
            'image_path' => null
        ];
        $response = $this->put(route('products.store'), $productData);
        $response->assertSee('The sale field is required.');
    }
    public function test_cant_store_product_if_not_have_cate()
    {
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $this->get(route('products.create'))->assertStatus(200);
        $productData = [
            'name' => 'New Product',
            'sale' => 0,
            'price' => 50.99,
            'image_path' => null
        ];
        $response = $this->put(route('products.store'), $productData);
        $response->assertSee('The categories field is required.');
    }
    public function test_cant_store_product_if_name_des_not_valid()
    {
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $this->get(route('products.create'))->assertStatus(200);
        $productData = [
            'name' => Str::random(300),
            'description' => Str::random(300),
            'sale' => 0,
            'price' => 100,
            'image_path' => null
        ];
        $response = $this->put(route('products.store'), $productData);
        $response->assertSee('The name must be less than 255 characters.');
        $response->assertSee('The description must be less than 255 characters.');
    }
    public function test_cant_store_product_if_sale_price_not_valid()
    {
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $this->get(route('products.create'))->assertStatus(200);
        $productData = [
            'name' => 'New Product',
            'description' => 'Product description',
            'sale' => -5,
            'price' => -10,
            'categories' => [1],
            'image_path' => null
        ];
        $response = $this->put(route('products.store'), $productData);
        $response->assertSee('The sale must be greater than or equal to 0.');
        $response->assertSee('The price must be a valid number format.');
    }
    public function test_cant_store_product_if_image_not_valid()
    {
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $this->get(route('products.create'))->assertStatus(200);
        $productData = [
            'name' => 'New Product',
            'description' => 'Product description',
            'sale' => 10,
            'price' => 10,
            'categories' => [1],
            'image_path' => '1'
        ];
        $response = $this->put(route('products.store'), $productData);
        $response->assertSee('Image must be right format');
    }
}
