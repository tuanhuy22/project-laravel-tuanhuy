<?php

namespace Tests\Feature\Product;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;

class UserGetProductTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_index_displays_product_list()
    {
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $product = Product::find(1);
        $response = $this->get(route('products.index'));
        $response->assertStatus(200);
        $response->assertViewIs('admin.product.index');
        $response->assertViewHas('products', $product);
    }
    public function test_cant_index_displays_product_list_if_not_login()
    {
        $response = $this->get(route('products.index'));
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }
}
