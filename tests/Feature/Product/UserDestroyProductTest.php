<?php

namespace Tests\Feature\Product;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;

class UserDestroyProductTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_can_destroy_product()
    {
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $product = Product::factory()->create();
        $response = $this->delete(route('products.destroy', ['product_id' => $product->id]));
        $response->assertStatus(200);
        $this->assertDeleted('products', ['id' => $product->id]);
    }

    public function test_cant_destroy_product_if_not_login()
    {
        $product = Product::factory()->create();
        $response = $this->delete(route('products.destroy', ['product_id' => $product->id]));
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }
}
