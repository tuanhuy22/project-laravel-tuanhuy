<?php

namespace Tests\Feature\Category;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;

class UserGetCategoryTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_index_displays_category_list()
    {
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $category = Category::find(1);
        $response = $this->get(route('categories.index'));
        $response->assertStatus(200);
        $response->assertViewIs('admin.category.index');
        $response->assertViewHas('categories', function ($categories) use ($category) {
            return $categories->contains($category);
        });
    }

    public function test_cant_index_displays_category_list_if_not_login()
    {
        $response = $this->get(route('categories.index'));
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }
}
