<?php

namespace Tests\Feature\Category;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Str;
use Tests\TestCase;
use App\Models\User;

class UserUpdateCategoryTest extends TestCase
{
    use WithFaker;

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_edit_displays_edit_form()
    {
        $product = Product::first();
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $category = Category::create([
            'name' => $this->faker->name,
            'products' => [$product->id],
            'parent_id' => 0
        ]);
        $response = $this->get(route('categories.edit', $category->id));
        $response->assertStatus(200);
        $response->assertViewIs('admin.category.edit');
        $response->assertViewHas('category', $category);
    }

    public function test_update_category()
    {
        $product = Product::first();
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $category = Category::create([
            'name' => $this->faker->name,
            'products' => [$product->id],
            'parent_id' => 0
        ]);
        $updatedData = [
            'name' => $this->faker->name,
            'products' => [$product->id]
        ];
        $response = $this->put(route('categories.update', $category->id), $updatedData);
        $response->assertRedirect(route('categories.index'));
    }

    public function test_cant_edit_displays_edit_form_if_not_login()
    {
        $product = Product::first();
        $category = Category::create([
            'name' => $this->faker->name,
            'products' => [$product->id],
            'parent_id' => 0
        ]);
        $response = $this->get(route('categories.edit', $category->id));
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }

    public function test_cant_update_updates_category_if_not_login()
    {
        $product = Product::first();
        $category = Category::create([
            'name' => $this->faker->name,
            'products' => [$product->id],
            'parent_id' => 0
        ]);
        $updatedData = [
            'name' => $this->faker->name,
            'products' => [$product->id],
            'parent_id' => 0
        ];
        $response = $this->put(route('categories.update', $category->id), $updatedData);
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }
    public function test_cant_update_category_if_name_not_valid()
    {
        $product = Product::first();
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $category = Category::create([
            'name' => $this->faker->name,
            'products' => [$product->id],
            'parent_id' => 0
        ]);
        $updatedData = [
            'name' => Str::random(1),
            'products' => [$product->id]
        ];
        $response = $this->post(route('categories.update', $category->id), $updatedData);
        $response->assertSee('The name must be at least 2 characters.');

    }
    public function test_cant_update_category_if_products_not_valid()
     {
         $product = Product::first();
         $user = User::where('email', 'tuanhuy227@gmail.com')->first();
         $this->actingAs($user);
         $category = Category::create([
             'name' => $this->faker->name,
             'products' => [$product->id],
             'parent_id' => 0
         ]);
         $updatedData = [
             'name' => Str::random(1),
             'products' => 0
         ];
         $response = $this->post(route('categories.update', $category->id), $updatedData);
         $response->assertSee('The products field is required.');
    }
}
