<?php

namespace Tests\Feature\Category;


use App\Models\Category;
use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;

class UserCreateCategoryTest extends TestCase
{
     use WithFaker;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_create_displays_create_form()
    {
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $response = $this->get(route('categories.create'));
        $response->assertStatus(200);
        $response->assertViewIs('admin.category.create');
    }
    public function test_store_category()
    {
        $product = Product::first();
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $categoryData = [
            'name' => $this->faker->name,
             'products' => [$product->id]
        ];
        $response = $this->post(route('categories.store'), $categoryData);
        $response->assertRedirect(route('categories.index'));
        $response->assertStatus(302);
        $response->assertSessionHas('success', 'Category created successfully');
    }

    public function test_cant_store_category_if_not_login()
    {
        $product = Product::first();
        $categoryData = [
            'name' => $this->faker->name,
            'products' => [$product->id]
        ];
        $response = $this->post(route('categories.store'), $categoryData);
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }
    public function test_cant_store_category_if_not_have_name()
    {
        $product = Product::first();
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $this->get(route('categories.create'))->assertStatus(200);
        $categoryData = [
            'products' => [$product->id]
        ];
        $response = $this->put(route('categories.store'), $categoryData);
        $response->assertSee('The name field is required.');
    }
    public function test_cant_store_category_if_not_have_product()
    {
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $this->get(route('categories.create'))->assertStatus(200);
        $categoryData = [
            'name' => $this->faker->name,
        ];
        $response = $this->put(route('categories.store'), $categoryData);
        $response->assertSee('The products field is required.');
    }


}
