<?php

namespace Tests\Feature\Category;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;

class UserDestroyCategoryTest extends TestCase
{
    use WithFaker;

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_can_destroy_category()
    {
        $product = Product::first();
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $category = Category::create([
            'name' => $this->faker->name,
            'products' => [$product->id],
            'parent_id' => 0
        ]);
        $response = $this->delete(route('categories.destroy', ['category_id' => $category->id]));
        $response->assertStatus(200);
        $this->assertDeleted('categories', ['id' => $category->id]);
    }

    public function test_cant_destroy_category_if_not_login()
    {
        $product = Product::first();
        $category = Category::create([
            'name' => $this->faker->name,
            'products' => [$product->id],
            'parent_id' => 0
        ]);
        $response = $this->delete(route('categories.destroy', ['category_id' => $category->id]));
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }
}
