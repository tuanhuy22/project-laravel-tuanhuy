<?php

namespace Tests\Feature\User;


use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Tests\TestCase;
use App\Models\User;

class CreateUserTest extends TestCase
{
    use WithFaker;

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_create_displays_create_form()
    {
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $response = $this->get(route('users.create'));
        $response->assertStatus(200);
        $response->assertViewIs('admin.user.create');
    }

    public function test_store_user()
    {
        $this->withoutExceptionHandling();
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $userData = [
            'name' => $this->faker->name(),
            'email' => $this->faker->unique()->safeEmail(),
            'phone' => $this->faker->regexify('^0[0-9]{9}$'),
            'password' => Hash::make(8),
            'roles' => [4]
        ];
        $response = $this->post(route('users.store'), $userData);
        $response->assertRedirect(route('users.index'));
        $response->assertStatus(302);
        $response->assertSessionHas('success', 'User created successfully. ');
    }

    public function test_cant_store_user_if_not_login()
    {
        $userData = [
            'name' => $this->faker->name(),
            'email' => $this->faker->unique()->safeEmail(),
            'phone' => $this->faker->regexify('^0[0-9]{9}$'),
            'password' => Hash::make(8),
            'roles' => [4]
        ];
        $response = $this->post(route('users.store'), $userData);
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }

    public function test_cant_store_user_if_not_have_phone()
    {
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $this->get(route('users.create'))->assertStatus(200);
        $userData = [
            'name' => $this->faker->name(),
            'email' => $this->faker->unique()->safeEmail(),
            'password' => Hash::make(8),
            'roles' => [4]
        ];
        $response = $this->put(route('users.store'), $userData);
        $response->assertSee('The phone field is required.');
    }
    public function test_cant_store_user_if_not_have_name()
    {
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $this->get(route('users.create'))->assertStatus(200);
        $userData = [
            'email' => $this->faker->unique()->safeEmail(),
            'phone' => $this->faker->regexify('^0[0-9]{9}$'),
            'password' => Hash::make(8),
            'roles' => [4]
        ];
        $response = $this->put(route('users.store'), $userData);
        $response->assertSee('The name field is required.');
    }
    public function test_cant_store_user_if__name_not_valid_max()
    {
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $this->get(route('users.create'))->assertStatus(200);
        $userData = [
             'name' => Str::random(300),
            'email' => $this->faker->unique()->safeEmail(),
            'phone' => $this->faker->regexify('^0[0-9]{9}$'),
            'password' => Hash::make(8),
            'roles' => [4]
        ];
        $response = $this->put(route('users.store'), $userData);
        $response->assertSee('The name must be less than 255 characters.');
    }
    public function test_cant_store_user_if_not_have_email()
    {
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $this->get(route('users.create'))->assertStatus(200);
        $userData = [
            'name' => $this->faker->name(),
            'email' => null,
            'phone' => $this->faker->regexify('^0[0-9]{9}$'),
            'password' => Hash::make(8),
            'roles' => [4]
        ];
        $response = $this->put(route('users.store'), $userData);
        $response->assertSee('The email field is required.');
    }
    public function test_cant_store_user_if_not_have_password()
    {
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $this->get(route('users.create'))->assertStatus(200);
        $userData = [
            'name' => $this->faker->name(),
            'roles' => [4]
        ];
        $response = $this->put(route('users.store'), $userData);
        $response->assertSee('The password field is required.');
    }
    public function test__cant_store_user_if_email_not_valid()
    {
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $userData = [
            'name' => $this->faker->name(),
            'email' => '......',
            'phone' => $this->faker->regexify('^0[0-9]{9}$'),
            'password' => Hash::make(8),
            'roles' => [4]
        ];
        $response = $this->put(route('users.store'), $userData);
        $response->assertSee('The email must right format');
    }
    public function test__cant_store_user_if_phone_not_valid()
    {
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $userData = [
            'name' => $this->faker->name(),
            'email' => $this->faker->unique()->safeEmail(),
            'phone' => Str::random(2),
            'password' => Hash::make(8),
            'roles' => [4]
        ];
        $response = $this->put(route('users.store'), $userData);
        $response->assertSee('The phone must right format');
    }
    public function test__cant_store_user_if_password_not_valid()
    {
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $userData = [
            'name' => $this->faker->name(),
            'email' => Str::random(8),
            'phone' => $this->faker->regexify('^0[0-9]{9}$'),
            'password' =>  Str::random(1),
            'roles' => [4]
        ];
        $response = $this->put(route('users.store'), $userData);
        $response->assertSee('The password must be at least 6 characters.');
    }
    public function test__cant_store_user_if_password_not_valid_max()
    {
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $userData = [
            'name' => $this->faker->name(),
            'email' => Str::random(8),
            'phone' => $this->faker->regexify('^0[0-9]{9}$'),
            'password' =>  Str::random(300),
            'roles' => [4]
        ];
        $response = $this->put(route('users.store'), $userData);
        $response->assertSee('The password must be less than 255 characters.');
    }
    public function test__cant_store_user_if_email_is_exits()
    {
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $userData = [
            'name' => $this->faker->name(),
            'email' => 'tuanhuy227@gmail.com',
            'phone' => $this->faker->regexify('^0[0-9]{9}$'),
            'password' => Hash::make(8),
            'roles' => [4]
        ];
        $response = $this->put(route('users.store'), $userData);
        $response->assertSee('The email has already been taken.');
    }
}
