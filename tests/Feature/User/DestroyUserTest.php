<?php

namespace Tests\Feature\User;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;

class DestroyUserTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_can_destroy_user()
    {
        $userAdmin = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($userAdmin);
        $user = User::factory()->create();
        $response = $this->delete(route('users.destroy', ['user_id' => $user->id]));
        $response->assertStatus(200);
        $this->assertDeleted('users', ['id' => $user->id]);
    }

    public function test_cant_destroy_user_if_not_login()
    {
        $user = User::factory()->create();
        $response = $this->delete(route('users.destroy', ['user_id' => $user->id]));
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }
}
