<?php

namespace Tests\Feature\User;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Str;
use Tests\TestCase;
use App\Models\User;

class UpdateUserTest extends TestCase
{
    use WithFaker;

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_edit_displays_edit_form()
    {
        $userAd = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($userAd);
        $user = User::factory()->create();
        $response = $this->get(route('users.edit', $user->id));
        $response->assertStatus(200);
        $response->assertViewIs('admin.user.edit');
        $response->assertViewHas('user', $user);
    }

    public function test_update_user()
    {
        $userAd = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($userAd);
        $users = User::factory()->create();
        $updatedData = [
            'name' => $this->faker->name(),
            'email' => $this->faker->unique()->safeEmail(),
            'phone' => '0978281798',
            'password' => Str::random(8),
            'roles' => [2]
        ];
        $response = $this->put(route('users.update', $users->id), $updatedData);
        $response->assertRedirect(route('users.index'));
    }

    public function test_cant_edit_displays_edit_form_if_not_login()
    {
        $users = User::factory()->create();
        $response = $this->get(route('users.edit', $users->id));
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }

    public function test_cant_update_updates_user_if_not_login()
    {
        $users = User::factory()->create();
        $updatedData = [
            'name' => $this->faker->name(),
            'email' => $this->faker->unique()->safeEmail(),
            'phone' => '0978281798',
            'password' => Str::random(8),
        ];
        $response = $this->put(route('users.update', $users->id), $updatedData);
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }
    public function test_cant_update_user_if_name_not_valid()
    {
        $userAd = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($userAd);
        $users = User::factory()->create();
        $updatedData = [
            'name' => Str::random(1),
            'email' => $this->faker->unique()->safeEmail(),
            'phone' => '0978281798',
            'password' => Str::random(8),
            'roles' => [2]
        ];
        $response = $this->post(route('users.update', $users->id), $updatedData);
        $response->assertSee('The name must be at least 3 characters.');
    }
    public function test_cant_update_user_if_phone_not_valid()
    {
        $userAd = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($userAd);
        $users = User::factory()->create();
        $updatedData = [
            'email' => $this->faker->unique()->safeEmail(),
            'phone' => Str::random(3),
            'password' => Str::random(8),
            'roles' => [2]
        ];
        $response = $this->post(route('users.update', $users->id), $updatedData);
        $response->assertSee('The phone must right format');
    }
    public function test_cant_update_user_if_email_not_valid()
    {
        $userAd = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($userAd);
        $users = User::factory()->create();
        $updatedData = [
            'email' => $this->faker->name,
            'phone' => Str::random(3),
            'password' => Str::random(8),
            'roles' => [2]
        ];
        $response = $this->post(route('users.update', $users->id), $updatedData);
        $response->assertSee('The email must right format');
    }
}
