<?php

namespace Tests\Feature\User;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;

class GetUserTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_index_displays_user_list()
    {
        $userAd = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($userAd);
        $user = User::find(1);
        $response = $this->get(route('users.index'));
        $response->assertStatus(200);
        $response->assertViewIs('admin.user.index');
        $response->assertViewHas('users', $user);
    }

    public function test_cant_index_displays_user_list_if_not_login()
    {
        $response = $this->get(route('users.index'));
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }
}
