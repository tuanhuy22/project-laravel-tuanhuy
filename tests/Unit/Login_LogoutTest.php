<?php

namespace Tests\Unit;

use App\Models\User;
use Tests\TestCase;


class Login_LogoutTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_user_can_login()
    {
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $response = $this->post(route('login'), [
            'email' => 'tuanhuy227@gmail.com',
            'password' => '12345678',
        ]);
        $response->assertStatus(302);
        $this->assertAuthenticatedAs($user);
        $response->assertRedirect(route('home'));
    }
    public function test_user_can_logout()
    {
        $user = User::where('email', 'tuanhuy227@gmail.com')->first();
        $this->actingAs($user);
        $response = $this->post('/logout');
        $response->assertRedirect('/login');
        $this->assertGuest();
    }
    public function test_user_cannot_login_with_incorrect_credentials()
    {
        $response = $this->post('/login', [
            'email' => 'test@example.com',
            'password' => 'incorrect-password',
        ]);
        $this->assertGuest();
    }

}
